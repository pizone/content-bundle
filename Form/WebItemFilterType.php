<?php

namespace PiZone\ContentBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class WebItemFilterType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('all', TextType::class, array(
                'label' => 'WEBITEM.FIELD.ALL',
                'required' => false
            ))
            ->add('alias', TextType::class, array(
                'label' => 'WEBITEM.FIELD.ALIAS',
                'required' => false
            ))
            ->add('description', TextType::class, array(
                'label' => 'WEBITEM.FIELD.DESCRIPTION',
                'required' => false
            ))
            ->add('content', TextType::class, array(
                'label' => 'WEBITEM.FIELD.CONTENT',
                'required' => false
            ))
            ->add('is_active',  ChoiceType::class, array(
                'choices' =>   array('WEBITEM.FIELD.IS_ACTIVE.NO' => 0,    'WEBITEM.FIELD.IS_ACTIVE.YES' => 1,),
                'label' => 'WEBITEM.FIELD.IS_ACTIVE.LABEL',
                'choices_as_values' => true,
                'placeholder' => 'WEBITEM.FIELD.IS_ACTIVE.ANY',
                'empty_data'  => null,
                'required' => false
            ))
        ;
    }
}
