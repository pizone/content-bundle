<?php

namespace PiZone\ContentBundle\Form;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class SectionFilterType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('all', TextType::class, array(
                'label' => 'SECTION.FIELD.ALL',
                'required' => false
            ))
            ->add('name', TextType::class, array(
                'label' => 'SECTION.FIELD.NAME',
                'required' => false
            ))
            ->add('alias', TextType::class, array(
                'label' => 'SECTION.FIELD.ALIAS',
                'required' => false
            ))
            ->add('description', TextType::class, array(
                'label' => 'SECTION.FIELD.DESCRIPTION',
                'required' => false
            ))
            ->add('anons', TextType::class, array(
                'label' => 'SECTION.FIELD.ANONS',
                'required' => false
            ))
            ->add('parent', EntityType::class, array(
                'label' => 'SECTION.FIELD.PARENT',
                'class' => 'PiZone\ContentBundle\Entity\Section',
                'required' => false
            ))
            ->add('is_active',  ChoiceType::class, array(
                'choices' =>   array('SECTION.FIELD.IS_ACTIVE.NO' => 0,    'SECTION.FIELD.IS_ACTIVE.YES' => 1,),
                'label' => 'SECTION.FIELD.IS_ACTIVE.LABEL',
                'choices_as_values' => true,
                'placeholder' => 'SECTION.FIELD.IS_ACTIVE.ANY',
                'empty_data'  => null,
                'required' => false
            ))

        ;
    }
}
