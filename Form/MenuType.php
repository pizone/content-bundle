<?php

namespace PiZone\ContentBundle\Form;


use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class MenuType extends AbstractType
{

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('is_active', CheckboxType::class, array(
                'label' => 'MENU.FIELD.IS_ACTIVE.LABEL'
            ))
            ->add('name', TextType::class, array(
                'label' => 'MENU.FIELD.NAME'
            ))
            ->add('alias', TextType::class, array(
                'label' => 'MENU.FIELD.ALIAS'
            ))
            ->add('items', CollectionType::class, array(
                'label' => 'MENU.FIELD.ITEM_LIST',
                'entry_type' => MenuItemType::class,
                'allow_add' => true,
                'allow_delete' => true
            ))
        ;

    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'validation_groups' => array('PZMenu'),
            'data_class' => 'PiZone\ContentBundle\Entity\Menu'
        ));
    }
}
