<?php

namespace PiZone\ContentBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class SectionMetaType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('meta_title', TextType::class, array(
                'label' => 'SECTION.FIELD.META_TITLE',
                'required' => false
            ))
            ->add('meta_keywords', TextType::class, array(
                'label' => 'SECTION.FIELD.META_KEYWORDS',
                'required' => false
            ))
            ->add('meta_description', TextareaType::class, array(
                'label' => 'SECTION.FIELD.META_DESCRIPTION',
                'required' => false
            ))
            ->add('more_scripts', TextareaType::class, array(
                'label' => 'SECTION.FIELD.META_SCRIPTS',
                'required' => false
            ))
        ;
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'validation_groups' => array('PZSection'),
            'data_class' => 'PiZone\ContentBundle\Entity\SectionMeta'
        ));
    }
}

