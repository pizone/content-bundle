<?php

namespace PiZone\ContentBundle\Form;

use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class LayoutFilterType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('all', TextType::class, array(
                'label' => 'LAYOUT.FIELD.ALL',
                'required' => false
            ))
            ->add('name', TextType::class, array(
                'label' => 'LAYOUT.FIELD.NAME',
                'required' => false
            ))
            ->add('description', TextType::class, array(
                'label' => 'LAYOUT.FIELD.DESCRIPTION',
                'required' => false
            ))
            ->add('file', TextType::class, array(
                'label' => 'LAYOUT.FIELD.FILE',
                'required' => false
            ))
            ->add('is_active',  ChoiceType::class, array(
                'choices' =>   array('LAYOUT.FIELD.IS_ACTIVE.NO' => 0,    'LAYOUT.FIELD.IS_ACTIVE.YES' => 1,),
                'label' => 'LAYOUT.FIELD.IS_ACTIVE.LABEL',
                'choices_as_values' => true,
                'placeholder' => 'LAYOUT.FIELD.IS_ACTIVE.ANY',
                'empty_data'  => null,
                'required' => false
            ))
            ->add(
                $builder->create('created_at', FormType::class, array(
                    'by_reference' => true,
                    'label' => 'LAYOUT.FIELD.CREATED_AT'
                ))
                    ->add('from', DateType::class, array(
                        'widget' => 'single_text',
                        'format' => 'dd.MM.yyyy',
                        'required' => false
                    ))
                    ->add('to', DateType::class, array(
                        'widget' => 'single_text',
                        'format' => 'dd.MM.yyyy',
                        'required' => false
                    ))
            )

        ;
    }
}
