<?php

namespace PiZone\ContentBundle\Form;


use PiZone\AdminBundle\Form\Type\EntityHiddenType;
use PiZone\ContentBundle\Entity\Repository\MenuItemRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;

class MenuItemType extends AbstractType
{

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('menu', EntityHiddenType::class, array(
                'class' => 'PiZone\ContentBundle\Entity\Menu',
                'em' => 'default'
            ))
            ->add('is_active', CheckboxType::class, array(
                'label' => 'MENU.FIELD.IS_ACTIVE.LABEL'
            ))
            ->add('title', TextType::class, array(
                'label' => 'MENU.FIELD.TITLE'
            ))
            ->add('alias', TextType::class, array(
                'label' => 'MENU.FIELD.ALIAS'
            ))
            ->add('link', TextType::class, array(
                'label' => 'MENU.FIELD.LINK'
            ));
        $data = $builder->getData();
        if($data && $menu = $data->getMenu()) {
            $builder->add('parent', EntityType::class, array(
                'label' => 'MENU.FIELD.PARENT',
                'class' => 'PiZone\ContentBundle\Entity\MenuItem',
                'query_builder' => function (MenuItemRepository $er) use ($data) {
                    $q = $er->createQueryBuilder('u')
                        ->andWhere('u.level <> 0')
                        ->andWhere('u.menu = :menu')
                        ->orderBy('u.lft', 'ASC')
                        ->setParameter('menu', $data->getMenu());
                    if($data->getId())
                        $q = $q->andWhere('u.id != :id')
                            ->andWhere('u.lft < :lft or u.rgt > :rgt')
                            ->setParameter('lft', $data->getLft())
                            ->setParameter('rgt', $data->getRgt())
                            ->setParameter('id', $data->getId());
                    return $q;
                }
            ));
        }
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'validation_groups' => array('PZMenuItem'),
            'allow_extra_fields' => true,
            'data_class' => 'PiZone\ContentBundle\Entity\MenuItem'
        ));
    }
}
