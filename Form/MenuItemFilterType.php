<?php

namespace PiZone\ContentBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class MenuItemFilterType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('all', TextType::class, array(
                'label' => 'MENU.FIELD.ALL',
                'required' => false
            ))
            ->add('title', TextType::class, array(
                'label' => 'MENU.FIELD.NAME',
                'required' => false
            ))
            ->add('alias', TextType::class, array(
                'label' => 'MENU.FIELD.ALIAS',
                'required' => false
            ))
            ->add('link', TextType::class, array(
                'label' => 'MENU.FIELD.LINK',
                'required' => false
            ))
            ->add('is_active',  ChoiceType::class, array(
                'choices' =>   array('MENU.FIELD.IS_ACTIVE.NO' => 0,    'MENU.FIELD.IS_ACTIVE.YES' => 1,),
                'label' => 'MENU.FIELD.IS_ACTIVE.LABEL',
                'choices_as_values' => true,
                'placeholder' => 'MENU.FIELD.IS_ACTIVE.ANY',
                'empty_data'  => null,
                'required' => false
            ))
        ;
    }
}
