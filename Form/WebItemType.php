<?php

namespace PiZone\ContentBundle\Form;


use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class WebItemType extends AbstractType
{

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('is_active', CheckboxType::class, array(
                'label' => 'WEBITEM.FIELD.IS_ACTIVE.LABEL'
            ))
            ->add('alias', TextType::class, array(
                'label' => 'WEBITEM.FIELD.ALIAS'
            ))
            ->add('description', TextType::class, array(
                'label' => 'WEBITEM.FIELD.DESCRIPTION'
            ))
            ->add('content', TextareaType::class, array(
                'label' => 'WEBITEM.FIELD.CONTENT'
            ))
            ->add('show_editor_content', CheckboxType::class, array(
                'required' => false,
                'label' =>'WEBITEM.FIELD.EDITOR_CONTENT'
            ))
        ;

    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'validation_groups' => array('PZWebItem'),
            'data_class' => 'PiZone\ContentBundle\Entity\WebItem'
        ));
    }
}
