<?php

namespace PiZone\ContentBundle\Form;


use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class LayoutType extends AbstractType
{

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('is_active', CheckboxType::class, array(
                'label' => 'LAYOUT.FIELD.IS_ACTIVE.LABEL'
            ))
            ->add('name', TextType::class, array(
                'label' => 'LAYOUT.FIELD.NAME'
            ))
            ->add('file', TextType::class, array(
                'label' => 'LAYOUT.FIELD.FILE'
            ))
            ->add('description', TextareaType::class, array(
                'label' => 'LAYOUT.FIELD.DESCRIPTION'
            ))
           ;

    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'validation_groups' => array('PZLayout'),
            'data_class' => 'PiZone\ContentBundle\Entity\Layout'
        ));
    }
}
