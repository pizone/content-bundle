<?php

namespace PiZone\ContentBundle\Form;


use PiZone\ContentBundle\Entity\Repository\SectionRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class SectionType extends AbstractType
{

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $data = $builder->getData();
        $builder
            ->add('is_active', CheckboxType::class, array(
                'label' => 'SECTION.FIELD.IS_ACTIVE.LABEL'
            ))
            ->add('parent', EntityType::class, array(
                'label' => 'SECTION.FIELD.PARENT',
                'class' => 'PiZone\ContentBundle\Entity\Section'
            ))
            ->add('parent', EntityType::class, array(
                'label' => 'SECTION.FIELD.PARENT',
                'class' => 'PiZone\ContentBundle\Entity\Section',
                'query_builder' => function (SectionRepository $er) use ($data) {
                    $q = $er->createQueryBuilder('u')
                        ->orderBy('u.lft', 'ASC');
                    if($data->getId())
                        $q = $q->andWhere('u.id != :id')
                            ->andWhere('u.lft < :lft or u.rgt > :rgt')
                            ->setParameter('lft', $data->getLft())
                            ->setParameter('rgt', $data->getRgt())
                            ->setParameter('id', $data->getId());
                    return $q;
                }
            ))
            ->add('name', TextType::class, array(
                'label' => 'SECTION.FIELD.NAME'
            ))
            ->add('alias', TextType::class, array(
                'label' => 'SECTION.FIELD.ALIAS'
            ))
            ->add('description', TextareaType::class, array(
                'label' => 'SECTION.FIELD.DESCRIPTION'
            ))
            ->add('show_editor_anons', CheckboxType::class, array(
                'required' => false,
                'label' => 'SECTION.FIELD.EDITOR_ANONS'
            ))
            ->add('anons', TextareaType::class, array(
                'label' => 'SECTION.FIELD.ANONS'
            ))
            ->add('image', FileType::class, array(
                'required' => false
            ))
            ->add('delete_image', CheckboxType::class, array(
                'label' => 'SECTION.FIELD.DELETE_IMAGE',
                'required' => false
            ))
            ->add('meta', SectionMetaType::class);
        ;

    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'validation_groups' => array('PZSection'),
            'data_class' => 'PiZone\ContentBundle\Entity\Section'
        ));
    }
}