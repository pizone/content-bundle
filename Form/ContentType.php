<?php

namespace PiZone\ContentBundle\Form;


use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ContentType extends AbstractType
{

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('is_active', CheckboxType::class, array(
                'label' => 'CONTENT.FIELD.IS_ACTIVE.LABEL'
            ))
            ->add('title', TextType::class, array(
                'label' => 'CONTENT.FIELD.TITLE'
            ))
            ->add('alias', TextType::class, array(
                'label' => 'CONTENT.FIELD.ALIAS'
            ))
            ->add('layout', EntityType::class, array(
                'label' => 'CONTENT.FIELD.LAYOUT',
                'class' => 'PiZone\ContentBundle\Entity\Layout'
            ))
            ->add('section', EntityType::class, array(
                'label' => 'CONTENT.FIELD.SECTION',
                'class' => 'PiZone\ContentBundle\Entity\Section'
            ))
            ->add('show_editor_anons', CheckboxType::class, array(
                'required' => false,
                'label' => 'CONTENT.FIELD.EDITOR_ANONS'
            ))
            ->add('anons', TextareaType::class, array(
                'label' => 'CONTENT.FIELD.ANONS'
            ))
            ->add('content', TextareaType::class, array(
                'label' => 'CONTENT.FIELD.CONTENT'
            ))
            ->add('show_editor_content', CheckboxType::class, array(
                'required' => false,
                'label' =>'CONTENT.FIELD.EDITOR_CONTENT'
            ))
            ->add('image', FileType::class, array(
                'required' => false
            ))
            ->add('big_image', FileType::class, array(
                'required' => false
            ))
            ->add('delete_image', CheckboxType::class, array(
                'label' => 'CONTENT.FIELD.DELETE_IMAGE',
                'required' => false
            ))
            ->add('delete_big_image', CheckboxType::class, array(
                'label' => 'CONTENT.FIELD.DELETE_IMAGE',
                'required' => false
            ))
            ->add('publish_at_from', DateTimeType::class, array(
                'label' => 'CONTENT.FIELD.PUBLISH_FROM',
                'widget' => 'single_text',
                'date_format' => 'dd.MM.yyyy  HH:mm',
                'format' => 'dd.MM.yyyy  HH:mm'
            ))
            ->add('publish_at_to', DateTimeType::class, array(
                'label' => 'CONTENT.FIELD.PUBLISH_TO',
                'widget' => 'single_text',
                'date_format' => 'dd.MM.yyyy  HH:mm',
                'format' => 'dd.MM.yyyy  HH:mm'
            ))
            ->add('meta', ContentMetaType::class);

    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'validation_groups' => array('PZContent'),
            'data_class' => 'PiZone\ContentBundle\Entity\Content'
        ));
    }
}
