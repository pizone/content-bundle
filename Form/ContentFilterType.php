<?php

namespace PiZone\ContentBundle\Form;

use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ContentFilterType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('all', TextType::class, array(
                'label' => 'CONTENT.FIELD.ALL',
                'required' => false
            ))
            ->add('title', TextType::class, array(
                'label' => 'CONTENT.FIELD.TITLE',
                'required' => false
            ))
            ->add('alias', TextType::class, array(
                'label' => 'CONTENT.FIELD.ALIAS',
                'required' => false
            ))
            ->add('anons', TextType::class, array(
                'label' => 'CONTENT.FIELD.ANONS',
                'required' => false
            ))
            ->add('section', EntityType::class, array(
                'label' => 'CONTENT.FIELD.SECTION',
                'class' => 'PiZone\ContentBundle\Entity\Section',
                'required' => false
            ))
            ->add('is_active',  ChoiceType::class, array(
                'choices' =>   array('CONTENT.FIELD.IS_ACTIVE.NO' => 0,    'CONTENT.FIELD.IS_ACTIVE.YES' => 1,),
                'label' => 'CONTENT.FIELD.IS_ACTIVE.LABEL',
                'choices_as_values' => true,
                'placeholder' => 'CONTENT.FIELD.IS_ACTIVE.ANY',
                'empty_data'  => null,
                'required' => false
            ))
            ->add(
                $builder->create('created_at', FormType::class, array(
                    'by_reference' => true,
                    'label' => 'CONTENT.FIELD.CREATED_AT'
                ))
                    ->add('from', DateType::class, array(
                        'widget' => 'single_text',
                        'format' => 'dd.MM.yyyy',
                        'required' => false
                    ))
                    ->add('to', DateType::class, array(
                        'widget' => 'single_text',
                        'format' => 'dd.MM.yyyy',
                        'required' => false
                    ))
            )
            ->add(
                $builder->create('updated_at', FormType::class, array(
                    'by_reference' => true,
                    'label' => 'CONTENT.FIELD.UPDATED_AT'
                ))
                    ->add('from', DateType::class, array(
                        'widget' => 'single_text',
                        'format' => 'dd.MM.yyyy',
                        'required' => false
                    ))
                    ->add('to', DateType::class, array(
                        'widget' => 'single_text',
                        'format' => 'dd.MM.yyyy',
                        'required' => false
                    ))
            )
            ->add(
                $builder->create('publish_at', FormType::class, array(
                    'by_reference' => true,
                    'label' => 'CONTENT.FIELD.PUBLISH_AT'
                ))
                    ->add('from', DateType::class, array(
                        'widget' => 'single_text',
                        'format' => 'dd.MM.yyyy',
                        'required' => false
                    ))
                    ->add('to', DateType::class, array(
                        'widget' => 'single_text',
                        'format' => 'dd.MM.yyyy',
                        'required' => false
                    ))
            )
        ;
    }
}
