<?php

namespace PiZone\ContentBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Gedmo\Mapping\Annotation as Gedmo;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Table(name="pz_section")
 * @Gedmo\Tree(type="nested")
 * @ORM\Entity(repositoryClass="PiZone\ContentBundle\Entity\Repository\SectionRepository")
 * @ORM\HasLifecycleCallbacks
 */
class Section
{

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    protected $name;

    /**
     * @ORM\Column(type="string", length=255)
     */
    protected $alias;

    /**
     * @ORM\Column(type="string", length=1000)
     */
    protected $description;

    /**
     * @ORM\OneToMany(targetEntity="PiZone\ContentBundle\Entity\Content", mappedBy="section", cascade={"persist", "remove"})
     */
    protected $content;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    protected $is_active = true;

    /**
     * @Gedmo\TreeLeft
     * @ORM\Column(type="integer")
     */
    private $lft;

    /**
     * @Gedmo\TreeRight
     * @ORM\Column(type="integer")
     */
    private $rgt;

    /**
     * @Gedmo\TreeParent
     * @ORM\ManyToOne(targetEntity="Section", inversedBy="children")
     * @ORM\JoinColumn(name="parent_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $parent;

    /**
     * @ORM\OneToMany(targetEntity="Section", mappedBy="parent")
     * @ORM\OrderBy({"lft" = "ASC"})
     */
    protected $children;

    /**
     * @Gedmo\TreeRoot
     * @ORM\Column(type="integer", nullable=true)
     */
    private $root;

    /**
     * @Gedmo\TreeLevel
     * @ORM\Column(name="lvl", type="integer")
     */
    private $level;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    protected $anons;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    protected $show_editor_anons = true;

    /**
     * @Assert\File(maxSize="6000000")
     */
    protected $image;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $image_path;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    protected $image_origin_name;

    protected $delete_image = false;

    private $temp_image;

    /**
     * @ORM\OneToOne(targetEntity="SectionMeta", mappedBy="section", cascade={"persist", "remove"})
     */
    protected $meta;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->content = new \Doctrine\Common\Collections\ArrayCollection();
        $this->children = new \Doctrine\Common\Collections\ArrayCollection();
    }

    public function __toString()
    {
        return $this->name;
    }

    public function getImageAbsolutePath()
    {
        return null === $this->image_path ? null : $this->getUploadRootDir().'/'.$this->image_path;
    }

    public function getImageWebPath() {
        return null === $this->image_path ? null : $this->getUploadDir() . '/' . $this->image_path;
    }

    protected function getUploadRootDir() {
        global $kernel;

        if ('AppCache' == get_class($kernel)) {
            $kernel = $kernel->getKernel();
        }

        return $kernel->getContainer()->getParameter('pi_zone_content.uploads.images.absolute_path');
    }

    protected function getUploadDir() {
        global $kernel;

        if ('AppCache' == get_class($kernel)) {
            $kernel = $kernel->getKernel();
        }

        return $kernel->getContainer()->getParameter('pi_zone_content.uploads.images.web_path');
    }

    /**
     * @ORM\PrePersist()
     * @ORM\PreUpdate()
     */
    public function preUpload() {
        if (null !== $this->getImage()) {
            // do whatever you want to generate a unique name
            $filename = sha1(uniqid(mt_rand(), true));
            $this->image_path = $filename.'.'.$this->getImage()->guessExtension();
            $this->image_origin_name = $this->getImage()->getClientOriginalName();
        }
    }

    /**
     * @ORM\PostPersist()
     * @ORM\PostUpdate()
     */
    public function upload() {
        if (null === $this->getImage()) {
            return;
        }

        if (null !== $this->getImage()){
            $this->getImage()->move($this->getUploadRootDir(), $this->image_path);
            if (isset($this->temp_image)) {
                // delete the old image
                unlink($this->getUploadRootDir().'/'.$this->temp_image);
                // clear the temp image path
                $this->temp_image = null;
            }
            $this->image = null;
        }
    }

    /**
     * @ORM\PostRemove()
     */
    public function removeUpload() {
        if ($file = $this->getImageAbsolutePath()) {
            if(file_exists($file))
                unlink($file);
        }
    }
    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Section
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Section
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set lft
     *
     * @param integer $lft
     *
     * @return Section
     */
    public function setLft($lft)
    {
        $this->lft = $lft;

        return $this;
    }

    /**
     * Get lft
     *
     * @return integer
     */
    public function getLft()
    {
        return $this->lft;
    }

    /**
     * Set rgt
     *
     * @param integer $rgt
     *
     * @return Section
     */
    public function setRgt($rgt)
    {
        $this->rgt = $rgt;

        return $this;
    }

    /**
     * Get rgt
     *
     * @return integer
     */
    public function getRgt()
    {
        return $this->rgt;
    }

    /**
     * Set root
     *
     * @param integer $root
     *
     * @return Section
     */
    public function setRoot($root)
    {
        $this->root = $root;

        return $this;
    }

    /**
     * Get root
     *
     * @return integer
     */
    public function getRoot()
    {
        return $this->root;
    }

    /**
     * Set level
     *
     * @param integer $level
     *
     * @return Section
     */
    public function setLevel($level)
    {
        $this->level = $level;

        return $this;
    }

    /**
     * Get level
     *
     * @return integer
     */
    public function getLevel()
    {
        return $this->level;
    }

    /**
     * Add content
     *
     * @param \PiZone\ContentBundle\Entity\Content $content
     *
     * @return Section
     */
    public function addContent(\PiZone\ContentBundle\Entity\Content $content)
    {
        $this->content[] = $content;

        return $this;
    }

    /**
     * Remove content
     *
     * @param \PiZone\ContentBundle\Entity\Content $content
     */
    public function removeContent(\PiZone\ContentBundle\Entity\Content $content)
    {
        $this->content->removeElement($content);
    }

    /**
     * Get content
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * Set parent
     *
     * @param \PiZone\ContentBundle\Entity\Section $parent
     *
     * @return Section
     */
    public function setParent(\PiZone\ContentBundle\Entity\Section $parent = null)
    {
        $this->parent = $parent;

        return $this;
    }

    /**
     * Get parent
     *
     * @return \PiZone\ContentBundle\Entity\Section
     */
    public function getParent()
    {
        return $this->parent;
    }

    /**
     * Add child
     *
     * @param \PiZone\ContentBundle\Entity\Section $child
     *
     * @return Section
     */
    public function addChild(\PiZone\ContentBundle\Entity\Section $child)
    {
        $this->children[] = $child;

        return $this;
    }

    /**
     * Remove child
     *
     * @param \PiZone\ContentBundle\Entity\Section $child
     */
    public function removeChild(\PiZone\ContentBundle\Entity\Section $child)
    {
        $this->children->removeElement($child);
    }

    /**
     * Get children
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getChildren()
    {
        return $this->children;
    }

    /**
     * Set isActive
     *
     * @param boolean $isActive
     *
     * @return Section
     */
    public function setIsActive($isActive)
    {
        $this->is_active = $isActive;

        return $this;
    }

    /**
     * Get isActive
     *
     * @return boolean
     */
    public function getIsActive()
    {
        return $this->is_active;
    }

    /**
     * Set alias
     *
     * @param string $alias
     *
     * @return Section
     */
    public function setAlias($alias)
    {
        $this->alias = $alias;

        return $this;
    }

    /**
     * Get alias
     *
     * @return string
     */
    public function getAlias()
    {
        return $this->alias;
    }

    /**
     * Sets file.
     *
     * @param UploadedFile $image
     */
    public function setImage(UploadedFile $image = null)
    {
        $this->image = $image;
        // check if we have an old image path
        if (isset($this->image_path)) {
            // store the old name to delete after the update
            $this->temp_image = $this->image_path;
            $this->image_path = null;
        } else {
            $this->image_path = 'initial';
        }
    }


    /**
     * Get image
     *
     * @return string
     */
    public function getImage()
    {
        return $this->image;
    }

    public function setDeleteImage($delete){
        if($delete && null === $this->getImage()){
            if ($file = $this->getImageAbsolutePath()) {
                $this->setImagePath(null);
                if(file_exists($file))
                    unlink($file);
            }
        }
    }

    public function getDeleteImage(){
        return $this->delete_image;
    }

    /**
     * Set anons
     *
     * @param string $anons
     *
     * @return Section
     */
    public function setAnons($anons)
    {
        $this->anons = $anons;

        return $this;
    }

    /**
     * Get anons
     *
     * @return string
     */
    public function getAnons()
    {
        return $this->anons;
    }

    /**
     * Set showEditorAnons
     *
     * @param boolean $showEditorAnons
     *
     * @return Section
     */
    public function setShowEditorAnons($showEditorAnons)
    {
        $this->show_editor_anons = $showEditorAnons;

        return $this;
    }

    /**
     * Get showEditorAnons
     *
     * @return boolean
     */
    public function getShowEditorAnons()
    {
        return $this->show_editor_anons;
    }

    /**
     * Set imagePath
     *
     * @param string $imagePath
     *
     * @return Section
     */
    public function setImagePath($imagePath)
    {
        $this->image_path = $imagePath;

        return $this;
    }

    /**
     * Get imagePath
     *
     * @return string
     */
    public function getImagePath()
    {
        return $this->image_path;
    }

    /**
     * Set imageOriginName
     *
     * @param string $imageOriginName
     *
     * @return Section
     */
    public function setImageOriginName($imageOriginName)
    {
        $this->image_origin_name = $imageOriginName;

        return $this;
    }

    /**
     * Get imageOriginName
     *
     * @return string
     */
    public function getImageOriginName()
    {
        return $this->image_origin_name;
    }

    /**
     * Set meta
     *
     * @param \PiZone\ContentBundle\Entity\SectionMeta $meta
     * @return Content
     */
    public function setMeta(\PiZone\ContentBundle\Entity\SectionMeta $meta = null)
    {
        $meta->setSection($this);
        $this->meta = $meta;

        return $this;
    }

    /**
     * Get meta
     *
     * @return \PiZone\ContentBundle\Entity\SectionMeta
     */
    public function getMeta()
    {
        return $this->meta;
    }
}
