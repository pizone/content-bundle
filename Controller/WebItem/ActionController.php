<?php

namespace PiZone\ContentBundle\Controller\WebItem;

use PiZone\AdminBundle\Controller\AActionController;

/**
 * WebItem controller.
 *
 */
class ActionController extends AActionController
{
    public function __construct(){
        $this->model = 'PiZone\ContentBundle\Entity\WebItem';
        $this->repository = 'PiZone\ContentBundle\Entity\WebItem';
        $this->route['delete'] = 'webitem_delete';
        $this->route['list']['name'] = 'webitem_type';
    }
}