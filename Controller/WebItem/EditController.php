<?php

namespace PiZone\ContentBundle\Controller\WebItem;

use PiZone\AdminBundle\Controller\AEditController;
use PiZone\AdminBundle\Controller\IEditController;

/**
 * WebItem controller.
 *
 */
class EditController extends AEditController implements IEditController
{
    public function __construct(){
        $this->model = 'PiZone\ContentBundle\Entity\WebItem';
        $this->form = 'PiZone\ContentBundle\Form\WebItemType';
        $this->routeList['update'] = 'webitem_update';
        $this->routeList['delete'] = 'webitem_delete';
    }
}