<?php

namespace PiZone\ContentBundle\Controller\WebItem;

use Doctrine\Common\Collections\ArrayCollection;
use PiZone\AdminBundle\Controller\AListController;
use PiZone\AdminBundle\Controller\IListController;
use Symfony\Component\HttpFoundation\Request;

/**
 * User list controller.
 *
 */
class ListController  extends AListController implements IListController
{
    protected $userId = null;

    public function __construct(){
        $this->prefixSession = 'PiZone\ContentBundle\WebItemList';
        $this->routeList = array(
            'list' => array(
                'name' => 'webitem',
                'parameters' => array()
            ),
            'delete' => 'webitem_delete',
            'batch' => array(
                'delete' => 'webitem_batch_delete',
                'active' => 'webitem_batch_active'
            )
        );
        $this->model = 'PiZone\ContentBundle\Entity\WebItem';
        $this->repository = 'PiZone\ContentBundle\Entity\WebItem';
        $this->filtersForm = 'PiZone\ContentBundle\Form\WebItemFilterType';
    }
    
    protected function getlist($entities){
        $tokenManager = $this->get('security.csrf.token_manager');

        foreach ($entities as $one) {
            $tokenId = $this->generateUrl($this->routeList['delete'], array('id' => $one->getId()));
            $token = $tokenManager->getToken($tokenId);

            $this->fieldList[] = array(
                'id' => $one->getId(),
                'alias' => $one->getAlias(),
                'description' => $one->getDescription(),
                'is_active' => $one->getIsActive(),
                '_token' => $token->getValue()
            );
        }

        return $this->fieldList;
    }

    protected function processFilters($query)
    {
        $filterObject = $this->getFilters();

        $queryFilter = $this->getQueryFilter();
        $queryFilter->setQuery($query);

        if (isset($filterObject['all']) && null !== $filterObject['all']) {
            $queryFilter->addOrStringFilter('alias', $filterObject['all']);
            $queryFilter->addOrStringFilter('description', $filterObject['all']);
            $queryFilter->addOrStringFilter('content', $filterObject['all']);
        }
        if (isset($filterObject['alias']) && null !== $filterObject['alias']) {
            $queryFilter->addStringFilter('alias', $filterObject['alias']);
        }
        if (isset($filterObject['description']) && null !== $filterObject['description']) {
            $queryFilter->addStringFilter('description', $filterObject['description']);
        }
        if (isset($filterObject['content']) && null !== $filterObject['content']) {
            $queryFilter->addStringFilter('content', $filterObject['content']);
        }
        if (isset($filterObject['is_active']) && null !== $filterObject['is_active']) {
            $queryFilter->addBooleanFilter('is_active', $filterObject['is_active']);
        }
    }
}