<?php

namespace PiZone\ContentBundle\Controller\WebItem;

use PiZone\AdminBundle\Controller\ANewController;
use PiZone\AdminBundle\Controller\INewController;
use Symfony\Component\HttpFoundation\Request;
use FOS\RestBundle\Util\Codes;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\View\View;

/**
 * WebItem controller.
 *
 */
class NewController extends ANewController implements INewController
{
    public function __construct(){
        $this->model = 'PiZone\ContentBundle\Entity\WebItem';
        $this->form = 'PiZone\ContentBundle\Form\WebItemType';
        $this->route['create'] = 'webitem_create';
    }
}
