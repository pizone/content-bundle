<?php

namespace PiZone\ContentBundle\Controller\Menu;

use PiZone\AdminBundle\Controller\AActionController;

/**
 * WebItem controller.
 *
 */
class ActionItemController extends AActionController
{
    public function __construct(){
        $this->model = 'PiZone\ContentBundle\Entity\MenuItem';
        $this->repository = 'PiZone\ContentBundle\Entity\MenuItem';
        $this->route['delete'] = 'menu_item_delete';
        $this->route['list']['name'] = 'menu_item';
    }

    protected function executeObjectDelete($entity)
    {
        $em = $this->getDoctrine()->getManager($this->manager);
        $em->removeFromTree($entity);
        $em->flush();
        $em->clear();
    }
}