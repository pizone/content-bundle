<?php

namespace PiZone\ContentBundle\Controller\Menu;

use PiZone\AdminBundle\Controller\AEditController;
use PiZone\AdminBundle\Controller\IEditController;

/**
 * WebItem controller.
 *
 */
class EditController extends AEditController implements IEditController
{
    public function __construct(){
        $this->model = 'PiZone\ContentBundle\Entity\Menu';
        $this->form = 'PiZone\ContentBundle\Form\MenuType';
        $this->routeList['update'] = 'menu_update';
        $this->routeList['delete'] = 'menu_delete';
    }
}