<?php

namespace PiZone\ContentBundle\Controller\Menu;

use PiZone\AdminBundle\Controller\ANewController;
use PiZone\AdminBundle\Controller\INewController;
use PiZone\ContentBundle\Entity\MenuItem;
use Symfony\Component\HttpFoundation\Request;
use FOS\RestBundle\Util\Codes;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\View\View;

/**
 * WebItem controller.
 *
 */
class NewItemController extends ANewController implements INewController
{
    protected $menu;

    public function __construct(){
        $this->model = 'PiZone\ContentBundle\Entity\MenuItem';
        $this->form = 'PiZone\ContentBundle\Form\MenuItemType';
        $this->route['create'] = 'menu_item_create';
    }

    protected function parseRequest(Request $request){
        $menuId = $request->query->get('menuId');
        if($menuId)
            $this->menu = $this->getDoctrine()->getRepository('PiZoneContentBundle:Menu')->find($menuId);
    }

    protected function getObject(){
        $item = new $this->model();
        if($this->menu)
            $item->setMenu($this->menu);

        return $item;
    }

    public function preSave($entity, $form){
        $menu = $entity->getMenu();
        if($menu->getItems()->count() == 0){
            $root = new MenuItem();
            $root->setMenu($menu);
            $root->setTitle('menu_' . $menu->getId());
            $root->setAlias('menu_' . $menu->getId());
            $root->setLink('/');
            $root->setIsActive(true);

            $em = $this->getDoctrine()->getManager();
            $em->persist($root);
        }
        else{
            $root = $this->getDoctrine()->getRepository($this->model)->findOneBy(array(
                'menu' => $menu,
                'alias' => 'menu_' . $menu->getId(),
                'level' => 0));
        }
        if(!$entity->getParent())
            $entity->setParent($root);

        return $entity;
    }
}
