<?php

namespace PiZone\ContentBundle\Controller\Menu;

use PiZone\AdminBundle\Controller\AEditController;
use PiZone\AdminBundle\Controller\IEditController;

/**
 * WebItem controller.
 *
 */
class EditItemController extends AEditController implements IEditController
{
    public function __construct(){
        $this->model = 'PiZone\ContentBundle\Entity\MenuItem';
        $this->form = 'PiZone\ContentBundle\Form\MenuItemType';
        $this->routeList['update'] = 'menu_item_update';
        $this->routeList['delete'] = 'menu_item_delete';
    }

    public function preSave($entity, $form){
        if(!$entity->getParent()){
            $menu = $entity->getMenu();
            $root = $this->getDoctrine()->getRepository($this->model)->findOneBy(array(
                'menu' => $menu,
                'alias' => 'menu_' . $menu->getId(),
                'level' => 0));
            $entity->setParent($root);
        }

        return $entity;
    }
}