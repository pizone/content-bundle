<?php

namespace PiZone\ContentBundle\Controller\Menu;

use PiZone\AdminBundle\Controller\AActionController;

/**
 * WebItem controller.
 *
 */
class ActionController extends AActionController
{
    public function __construct(){
        $this->model = 'PiZone\ContentBundle\Entity\Menu';
        $this->repository = 'PiZone\ContentBundle\Entity\Menu';
        $this->route['delete'] = 'menu_delete';
        $this->route['list']['name'] = 'menu';
    }
}