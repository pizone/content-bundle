<?php

namespace PiZone\ContentBundle\Controller\Menu;

use Doctrine\Common\Collections\ArrayCollection;
use PiZone\AdminBundle\Controller\AListController;
use PiZone\AdminBundle\Controller\IListController;
use Symfony\Component\HttpFoundation\Request;

/**
 * User list controller.
 *
 */
class ListController  extends AListController implements IListController
{
    protected $userId = null;

    public function __construct(){
        $this->prefixSession = 'PiZone\ContentBundle\MenuList';
        $this->routeList = array(
            'list' => array(
                'name' => 'menu',
                'parameters' => array()
            ),
            'delete' => 'menu_delete',
            'batch' => array(
                'delete' => 'menu_batch_delete',
                'active' => 'menu_batch_active'
            )
        );
        $this->model = 'PiZone\ContentBundle\Entity\Menu';
        $this->repository = 'PiZone\ContentBundle\Entity\Menu';
        $this->filtersForm = 'PiZone\ContentBundle\Form\MenuFilterType';
    }
    
    protected function getlist($entities){
        $tokenManager = $this->get('security.csrf.token_manager');

        foreach ($entities as $one) {
            $tokenId = $this->generateUrl($this->routeList['delete'], array('id' => $one->getId()));
            $token = $tokenManager->getToken($tokenId);

            $this->fieldList[] = array(
                'id' => $one->getId(),
                'name' => $one->getName(),
                'alias' => $one->getAlias(),
                'is_active' => $one->getIsActive(),
                '_token' => $token->getValue()
            );
        }

        return $this->fieldList;
    }

    protected function processFilters($query)
    {
        $filterObject = $this->getFilters();

        $queryFilter = $this->getQueryFilter();
        $queryFilter->setQuery($query);

        if (isset($filterObject['all']) && null !== $filterObject['all']) {
            $queryFilter->addOrStringFilter('name', $filterObject['all']);
            $queryFilter->addOrStringFilter('alias', $filterObject['all']);
        }
        if (isset($filterObject['name']) && null !== $filterObject['name']) {
            $queryFilter->addStringFilter('name', $filterObject['name']);
        }
        if (isset($filterObject['alias']) && null !== $filterObject['alias']) {
            $queryFilter->addStringFilter('alias', $filterObject['alias']);
        }
        if (isset($filterObject['is_active']) && null !== $filterObject['is_active']) {
            $queryFilter->addBooleanFilter('is_active', $filterObject['is_active']);
        }
    }
}