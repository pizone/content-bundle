<?php

namespace PiZone\ContentBundle\Controller\Menu;

use Doctrine\Common\Collections\ArrayCollection;
use PiZone\AdminBundle\Controller\AListController;
use PiZone\AdminBundle\Controller\IListController;
use Symfony\Component\HttpFoundation\Request;

/**
 * User list controller.
 *
 */
class ListItemController  extends AListController implements IListController
{
    protected $menu = null;

    public function __construct(){
        $this->prefixSession = 'PiZone\ContentBundle\MenuItemList';
        $this->routeList = array(
            'list' => array(
                'name' => 'menu_item',
                'parameters' => array()
            ),
            'delete' => 'menu_item_delete',
            'batch' => array(
                'delete' => 'menu_item_batch_delete',
                'active' => 'menu_item_batch_active'
            )
        );
        $this->model = 'PiZone\ContentBundle\Entity\MenuItem';
        $this->repository = 'PiZone\ContentBundle\Entity\MenuItem';
        $this->filtersForm = 'PiZone\ContentBundle\Form\MenuItemFilterType';
    }

    protected function parseRequest(Request $request){
        $menuId = $request->query->get('menuId');

        $this->menu = $this->getDoctrine()->getRepository('PiZoneContentBundle:Menu')->find($menuId);
    }
    
    protected function getlist($entities){
        $tokenManager = $this->get('security.csrf.token_manager');

        foreach ($entities as $one) {
            $tokenId = $this->generateUrl($this->routeList['delete'], array('id' => $one->getId()));
            $token = $tokenManager->getToken($tokenId);

            $this->fieldList[] = array(
                'id' => $one->getId(),
                'title' => $one->getTitle(),
                'alias' => $one->getAlias(),
                'link' => $one->getLink(),
                'level' => $one->getLevel(),
                'lft' => $one->getLft(),
                'is_active' => $one->getIsActive(),
                '_token' => $token->getValue()
            );
        }

        return $this->fieldList;
    }

    protected function processQuery($query)
    {
        $query->andWhere('q.menu = :menu')
            ->andWhere('q.level <> 0')
            ->setParameter('menu', $this->menu)
            ->addOrderBy('q.lft');
        return $query;
    }

    protected function processFilters($query)
    {
        $filterObject = $this->getFilters();

        $queryFilter = $this->getQueryFilter();
        $queryFilter->setQuery($query);

        if (isset($filterObject['all']) && null !== $filterObject['all']) {
            $queryFilter->addOrStringFilter('title', $filterObject['all']);
            $queryFilter->addOrStringFilter('alias', $filterObject['all']);
            $queryFilter->addOrStringFilter('link', $filterObject['all']);
        }
        if (isset($filterObject['title']) && null !== $filterObject['title']) {
            $queryFilter->addStringFilter('title', $filterObject['title']);
        }
        if (isset($filterObject['alias']) && null !== $filterObject['alias']) {
            $queryFilter->addStringFilter('alias', $filterObject['alias']);
        }
        if (isset($filterObject['link']) && null !== $filterObject['link']) {
            $queryFilter->addStringFilter('link', $filterObject['link']);
        }
        if (isset($filterObject['is_active']) && null !== $filterObject['is_active']) {
            $queryFilter->addBooleanFilter('is_active', $filterObject['is_active']);
        }
    }
}