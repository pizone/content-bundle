<?php

namespace PiZone\ContentBundle\Controller\Layout;

use Doctrine\Common\Collections\ArrayCollection;
use PiZone\AdminBundle\Controller\AListController;
use PiZone\AdminBundle\Controller\IListController;
use Symfony\Component\HttpFoundation\Request;

/**
 * User list controller.
 *
 */
class ListController  extends AListController implements IListController
{
    protected $userId = null;

    public function __construct(){
        $this->prefixSession = 'PiZone\ContentBundle\LayoutList';
        $this->routeList = array(
            'list' => array(
                'name' => 'layout',
                'parameters' => array()
            ),
            'delete' => 'layout_delete',
            'batch' => array(
                'delete' => 'layout_batch_delete',
                'active' => 'layout_batch_active'
            )
        );
        $this->model = 'PiZone\ContentBundle\Entity\Layout';
        $this->repository = 'PiZone\ContentBundle\Entity\Layout';
        $this->filtersForm = 'PiZone\ContentBundle\Form\LayoutFilterType';
    }
    
    protected function getlist($entities){
        $tokenManager = $this->get('security.csrf.token_manager');

        foreach ($entities as $one) {
            $tokenId = $this->generateUrl($this->routeList['delete'], array('id' => $one->getId()));
            $token = $tokenManager->getToken($tokenId);

            $this->fieldList[] = array(
                'id' => $one->getId(),
                'name' => $one->getName(),
                'description' => $one->getDescription(),
                'file' => $one->getFile(),
                'is_active' => $one->getIsActive(),
                '_token' => $token->getValue()
            );
        }

        return $this->fieldList;
    }

    protected function processFilters($query)
    {
        $filterObject = $this->getFilters();

        $queryFilter = $this->getQueryFilter();
        $queryFilter->setQuery($query);

        if (isset($filterObject['all']) && null !== $filterObject['all']) {
            $queryFilter->addOrStringFilter('name', $filterObject['all']);
            $queryFilter->addOrStringFilter('description', $filterObject['all']);
            $queryFilter->addOrStringFilter('file', $filterObject['all']);
        }
        if (isset($filterObject['name']) && null !== $filterObject['name']) {
            $queryFilter->addStringFilter('name', $filterObject['name']);
        }
        if (isset($filterObject['description']) && null !== $filterObject['description']) {
            $queryFilter->addStringFilter('description', $filterObject['description']);
        }
        if (isset($filterObject['file']) && null !== $filterObject['file']) {
            $queryFilter->addStringFilter('file', $filterObject['file']);
        }
        if (isset($filterObject['is_active']) && null !== $filterObject['is_active']) {
            $queryFilter->addBooleanFilter('is_active', $filterObject['is_active']);
        }
    }
}