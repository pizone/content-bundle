<?php

namespace PiZone\ContentBundle\Controller\Layout;

use PiZone\AdminBundle\Controller\AEditController;
use PiZone\AdminBundle\Controller\IEditController;

/**
 * WebItem controller.
 *
 */
class EditController extends AEditController implements IEditController
{
    public function __construct(){
        $this->model = 'PiZone\ContentBundle\Entity\Layout';
        $this->form = 'PiZone\ContentBundle\Form\LayoutType';
        $this->routeList['update'] = 'layout_update';
        $this->routeList['delete'] = 'layout_delete';
    }
}