<?php

namespace PiZone\ContentBundle\Controller\Layout;

use PiZone\AdminBundle\Controller\ANewController;
use PiZone\AdminBundle\Controller\INewController;
use Symfony\Component\HttpFoundation\Request;
use FOS\RestBundle\Util\Codes;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\View\View;

/**
 * WebItem controller.
 *
 */
class NewController extends ANewController implements INewController
{
    public function __construct(){
        $this->model = 'PiZone\ContentBundle\Entity\Layout';
        $this->form = 'PiZone\ContentBundle\Form\LayoutType';
        $this->route['create'] = 'layout_create';
    }
}
