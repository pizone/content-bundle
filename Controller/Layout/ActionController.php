<?php

namespace PiZone\ContentBundle\Controller\Layout;

use PiZone\AdminBundle\Controller\AActionController;

/**
 * WebItem controller.
 *
 */
class ActionController extends AActionController
{
    public function __construct(){
        $this->model = 'PiZone\ContentBundle\Entity\Layout';
        $this->repository = 'PiZone\ContentBundle\Entity\Layout';
        $this->route['delete'] = 'layout_delete';
        $this->route['list']['name'] = 'layout';
    }
}