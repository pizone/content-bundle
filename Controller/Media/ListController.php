<?php

namespace PiZone\ContentBundle\Controller\Media;

use FOS\RestBundle\Controller\FOSRestController;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\Request;

/**
 * User list controller.
 *
 */
class ListController  extends FOSRestController {

    protected $query;
    protected $type;

    public function indexAction(Request $request){
        if($request->query->get('type'))
            $this->type = $request->query->get('type');
        if($request->query->get('q'))
            $this->query = $request->query->get('q');

        $path = $this->getParameter('uploads');

        $files = array();

        $arrFiles = array_diff( scandir( $path), array('..', '.'));

        foreach ($arrFiles as $one){
            if(is_dir($one)){
                $files = $this->getFilesFromDir($path, $files);
            }
            else{
                $files = $this->getImages($path, $one, $files);
            }
        }

        $view = $this->view($files)
            ->setTemplate('PiZoneAdminBundle:Admin:_data.yml.twig');

        return $this->handleView($view);
    }

    private function getFilesFromDir($path, $files){
        $arrFiles = scandir($path);

        foreach ($arrFiles as $one){
            if(is_dir($path .$one)){
                $files = $this-> getFilesFromDir($path . $one, $files);
            }
            else{
                $files = $this->getImages($path, $one, $files);
            }
        }

        return $files;
    }

    private function getImages($path, $one, $files){
        if($one != '.gitkeep') {
            $info = pathinfo($path . '/' . $one);

            $ext = $info['extension'];

//            if ($this->type == 'image') {
//                if ($ext == 'jpeg' || $ext == 'jpg' || $ext == 'png' || $ext == 'jif') {
                    if (!$this->query || stripos($one, $this->query) !== false) {
                        $files[] = array(
                            "imageUrl" => '/uploads/' . $one,
                            "name" => $info['basename'],
                            "value" => '/uploads/' . $one
                        );
                    }
//                }
//            }
//            elseif ($this->type == 'media') {
//                if (!$this->query || stripos($one, $this->query) !== false) {
//                    $files[] = array(
//                        "imageUrl" => '/uploads/media/' . $one,
//                        "name" => $info['basename'],
//                        "value" => '/uploads/media/' . $one
//                    );
//                }
//            } else {
//                if (!$this->query || stripos($one, $this->query) !== false) {
//                    $files[] = array(
//                        "imageUrl" => '/uploads/docs/' . $one,
//                        "name" => $info['basename'],
//                        "value" => '/uploads/docs/' . $one
//                    );
//                }
//            }
        }

        return $files;
    }

    public function uploadFileAction(Request $request){
        $file = $request->files->get('file');

        $path = $this->getParameter('uploads');

        try {
            $file->move($path, $file->getClientOriginalName());
            $view = $this->view(json_encode(array('result' => 'ok')))
                ->setTemplate('PiZoneAdminBundle:Admin:_data.yml.twig');

            return $this->handleView($view);
        }
        catch (FileException $error){
            $view = $this->view(json_encode(array('result' => 'error', 'mess' => $error->getMessage())))
                ->setTemplate('PiZoneAdminBundle:Admin:_data.yml.twig');

            return $this->handleView($view);
        }
    }
}

