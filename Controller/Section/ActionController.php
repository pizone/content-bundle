<?php

namespace PiZone\ContentBundle\Controller\Section;

use PiZone\AdminBundle\Controller\AActionController;

/**
 * WebItem controller.
 *
 */
class ActionController extends AActionController
{
    public function __construct(){
        $this->model = 'PiZone\ContentBundle\Entity\Section';
        $this->repository = 'PiZone\ContentBundle\Entity\Section';
        $this->route['delete'] = 'section_delete';
        $this->route['list']['name'] = 'section_type';
    }

    protected function executeObjectDelete($entity)
    {
        $em = $this->getDoctrine()->getManager($this->manager);
        $em->removeFromTree($entity);
        $em->flush();
        $em->clear();
    }
}