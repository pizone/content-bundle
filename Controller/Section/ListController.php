<?php

namespace PiZone\ContentBundle\Controller\Section;

use Doctrine\Common\Collections\ArrayCollection;
use PiZone\AdminBundle\Controller\AListController;
use PiZone\AdminBundle\Controller\IListController;
use Symfony\Component\HttpFoundation\Request;

/**
 * User list controller.
 *
 */
class ListController  extends AListController implements IListController
{
    protected $userId = null;

    public function __construct(){
        $this->prefixSession = 'PiZone\ContentBundle\SectionList';
        $this->routeList = array(
            'list' => array(
                'name' => 'section',
                'parameters' => array()
            ),
            'delete' => 'section_delete',
            'batch' => array(
                'delete' => 'section_batch_delete',
                'active' => 'section_batch_active'
            )
        );
        $this->model = 'PiZone\ContentBundle\Entity\Section';
        $this->repository = 'PiZone\ContentBundle\Entity\Section';
        $this->filtersForm = 'PiZone\ContentBundle\Form\SectionFilterType';
    }
    
    protected function getlist($entities){
        $tokenManager = $this->get('security.csrf.token_manager');

        foreach ($entities as $one) {
            $tokenId = $this->generateUrl($this->routeList['delete'], array('id' => $one->getId()));
            $token = $tokenManager->getToken($tokenId);

            $this->fieldList[] = array(
                'id' => $one->getId(),
                'name' => $one->getName(),
                'alias' => $one->getAlias(),
                'description' => $one->getDescription(),
                'anons' => $one->getAnons(),
                'image' => $one->getImageWebPath(),
                'is_active' => $one->getIsActive(),
                'level' => $one->getLevel() + 1,
                '_token' => $token->getValue()
            );
        }

        return $this->fieldList;
    }

    protected function processQuery($query)
    {
        $query->addOrderBy('q.lft');
        return $query;
    }

    protected function processFilters($query)
    {
        $filterObject = $this->getFilters();

        $queryFilter = $this->getQueryFilter();
        $queryFilter->setQuery($query);

        if (isset($filterObject['all']) && null !== $filterObject['all']) {
            $queryFilter->addOrStringFilter('name', $filterObject['all']);
            $queryFilter->addOrStringFilter('description', $filterObject['all']);
        }
        if (isset($filterObject['name']) && null !== $filterObject['name']) {
            $queryFilter->addStringFilter('name', $filterObject['name']);
        }
        if (isset($filterObject['alias']) && null !== $filterObject['alias']) {
            $queryFilter->addStringFilter('alias', $filterObject['alias']);
        }
        if (isset($filterObject['description']) && null !== $filterObject['description']) {
            $queryFilter->addStringFilter('description', $filterObject['description']);
        }
        if (isset($filterObject['parent']) && null !== $filterObject['parent']) {
//            $queryFilter->addStringFilter('description', $filterObject['description']);
        }
        if (isset($filterObject['is_active']) && null !== $filterObject['is_active']) {
            $queryFilter->addBooleanFilter('is_active', $filterObject['is_active']);
        }
    }
}