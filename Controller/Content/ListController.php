<?php

namespace PiZone\ContentBundle\Controller\Content;

use Doctrine\Common\Collections\ArrayCollection;
use PiZone\AdminBundle\Controller\AListController;
use PiZone\AdminBundle\Controller\IListController;
use Symfony\Component\HttpFoundation\Request;

/**
 * User list controller.
 *
 */
class ListController  extends AListController implements IListController
{
    protected $userId = null;

    public function __construct(){
        $this->prefixSession = 'PiZone\ContentBundle\ContentList';
        $this->routeList = array(
            'list' => array(
                'name' => 'content',
                'parameters' => array()
            ),
            'delete' => 'content_delete',
            'batch' => array(
                'delete' => 'content_batch_delete',
                'active' => 'content_batch_active'
            )
        );
        $this->model = 'PiZone\ContentBundle\Entity\Content';
        $this->repository = 'PiZone\ContentBundle\Entity\Content';
        $this->filtersForm = 'PiZone\ContentBundle\Form\ContentFilterType';
    }
    
    protected function getlist($entities){
        $tokenManager = $this->get('security.csrf.token_manager');

        foreach ($entities as $one) {
            $tokenId = $this->generateUrl($this->routeList['delete'], array('id' => $one->getId()));
            $token = $tokenManager->getToken($tokenId);

            $this->fieldList[] = array(
                'id' => $one->getId(),
                'title' => $one->getTitle(),
                'alias' => $one->getAlias(),
                'anons' => $one->getAnons(),
                'layout' => $one->getLayout() ? $one->getLayout()->getName() : '',
                'section' => $one->getSection() ? $one->getSection()->getName() : '',
                'image' => $one->getImageWebPath(),
                'publish_at_from' => $one->getPublishAtFrom() ? $one->getPublishAtFrom()->format('d.m.Y H:i') : '',
                'publish_at_to' => $one->getPublishAtTo() ? $one->getPublishAtTo()->format('d.m.Y H:i') : '',
                'created_at' => $one->getCreatedAt() ? $one->getCreatedAt()->format('d.m.Y H:i:s') : '',
                'updated_at' => $one->getUpdatedAt() ? $one->getUpdatedAt()->format('d.m.Y H:i:s') : '',
                'is_active' => $one->getIsActive(),
                '_token' => $token->getValue()
            );
        }

        return $this->fieldList;
    }

    protected function getFilterForm()
    {
        $filters = $this->getFilters();
        if(isset($filters['section']) && $filters['section']) {
            $em = $this->getDoctrine()->getManager($this->manager);
            $em->persist($filters['section']);
        }

        return $this->createForm($this->getFiltersType(), $filters);
    }

    protected function processFilters($query)
    {
        $filterObject = $this->getFilters();

        $queryFilter = $this->getQueryFilter();
        $queryFilter->setQuery($query);


        if (isset($filterObject['all']) && null !== $filterObject['all']) {
            $queryFilter->addOrStringFilter('title', $filterObject['all']);
            $queryFilter->addOrStringFilter('alias', $filterObject['all']);
            $queryFilter->addOrStringFilter('anons', $filterObject['all']);
        }
        if (isset($filterObject['title']) && null !== $filterObject['title']) {
            $queryFilter->addStringFilter('title', $filterObject['title']);
        }
        if (isset($filterObject['alias']) && null !== $filterObject['alias']) {
            $queryFilter->addStringFilter('alias', $filterObject['alias']);
        }
        if (isset($filterObject['anons']) && null !== $filterObject['anons']) {
            $queryFilter->addStringFilter('anons', $filterObject['anons']);
        }
        if (isset($filterObject['is_active']) && null !== $filterObject['is_active']) {
            $queryFilter->addBooleanFilter('is_active', $filterObject['is_active']);
        }
        if (isset($filterObject['created_at']) && null !== $filterObject['created_at']) {
            $queryFilter->addDateFilter('created_at', $filterObject['created_at']);
        }
        if (isset($filterObject['updated_at']) && null !== $filterObject['updated_at']) {
            $queryFilter->addDateFilter('updated_at', $filterObject['updated_at']);
        }
        if (isset($filterObject['section']) && null !== $filterObject['section']) {
            $queryFilter->addDefaultFilter('section', $filterObject['section']);
        }
        if (isset($filterObject['publish_at']) && null !== $filterObject['publish_at']) {
            $this->addPublishFilter($queryFilter, $filterObject['publish_at']);
        }
    }

    public function addPublishFilter($queryFilter, $value){
        $query = $queryFilter->getQuery();
        if($value['to'])
            $query->andWhere('q.publish_at_from IS NULL OR q.publish_at_from <= :publish_at_to')
                ->setParameter('publish_at_to', $value['to']);
        if($value['from'])
            $query->andWhere('q.publish_at_to IS NULL OR :publish_at_from <= q.publish_at_to')
                ->setParameter('publish_at_from', $value['from']);

        return $queryFilter->setQuery($query);
    }
}