<?php

namespace PiZone\ContentBundle\Controller\Content;

use PiZone\AdminBundle\Controller\AEditController;
use PiZone\AdminBundle\Controller\IEditController;

/**
 * WebItem controller.
 *
 */
class EditController extends AEditController implements IEditController
{
    public function __construct(){
        $this->model = 'PiZone\ContentBundle\Entity\Content';
        $this->form = 'PiZone\ContentBundle\Form\ContentType';
        $this->routeList['update'] = 'content_update';
        $this->routeList['delete'] = 'content_delete';
    }

    public function prepareView($id, $editForm){
        $data = $this->get('pz_form')->formDataToArray($editForm->createView());

        if($editForm->getData()->getImageWebPath()){
            $info = pathinfo($editForm->getData()->getImageAbsolutePath());

            $data['image']['info'] = array(
                'path' => $editForm->getData()->getImageWebPath(),
                'size' => file_exists($editForm->getData()->getImageAbsolutePath()) ? filesize($editForm->getData()->getImageAbsolutePath()) : 0,
                'name' => $info ? $info['basename'] : '',
                'mimetype' => $info ? $info['extension'] : ''
            );
        }

        if($editForm->getData()->getBigImageWebPath()){
            $info = pathinfo($editForm->getData()->getBigImageAbsolutePath());

            $data['big_image']['info'] = array(
                'path' => $editForm->getData()->getBigImageWebPath(),
                'size' => file_exists($editForm->getData()->getBigImageAbsolutePath()) ? filesize($editForm->getData()->getBigImageAbsolutePath()) : 0,
                'name' => $info ? $info['basename'] : '',
                'mimetype' => $info ? $info['extension'] : ''
            );
        }

        return array(
            'action' => $this->generateUrl($this->routeList['update'], array('id' => $id)),
            'fields' => $data,
            '_delete_token' => $this->getDeleteFormToken($id)
        );
    }
}