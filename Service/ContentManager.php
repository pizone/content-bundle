<?php

namespace PiZone\ContentBundle\Service;

class ContentManager{
    /**
     * @var ContainerInterface
     */
    protected $container;
    protected $doctrine;

    /**
     * Constructor
     *
     * @param ContainerInterface $container
     */
    public function __construct($container) {
        $this->container = $container;
        $this->doctrine = $container->get('doctrine');
    }

    public function getContentByAlias($alias){
        return $this->doctrine->getRepository('PiZoneContentBundle:Content')->findContentByAlias($alias);
    }

    public function getSectionContentByAlias($section, $alias){
        return $this->doctrine->getRepository('PiZoneContentBundle:Content')->findSectionContentByAlias($section, $alias);
    }
}