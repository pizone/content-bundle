<?php

namespace PiZone\ContentBundle\Service;

class WebItemManager{
    /**
     * @var ContainerInterface
     */
    protected $container;
    protected $doctrine;

    /**
     * Constructor
     *
     * @param ContainerInterface $container
     */
    public function __construct($container) {
        $this->container = $container;
        $this->doctrine = $container->get('doctrine');
    }

    public function getContentByAlias($alias){
        return $this->doctrine->getRepository('PiZoneContentBundle:WebItem')->findContentByAlias($alias);
    }
}