<?php

namespace PiZone\ContentBundle\Service;

class MenuManager
{
    /**
     * @var ContainerInterface
     */
    protected $container;
    protected $doctrine;

    /**
     * Constructor
     *
     * @param ContainerInterface $container
     */
    public function __construct($container)
    {
        $this->container = $container;
        $this->doctrine = $container->get('doctrine');
    }

    public function GetMenuItemsByMenuAlias($alias){
        return $this->doctrine->getRepository('PiZoneContentBundle:MenuItem')->findMenuItemsByMenuAlias($alias);
    }
}