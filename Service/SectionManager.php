<?php

namespace PiZone\ContentBundle\Service;

class SectionManager{
    /**
     * @var ContainerInterface
     */
    protected $container;
    protected $doctrine;

    /**
     * Constructor
     *
     * @param ContainerInterface $container
     */
    public function __construct($container) {
        $this->container = $container;
        $this->doctrine = $container->get('doctrine');
    }

    public function GetSectionByAlias($alias){
        return $this->doctrine->getRepository('PiZoneContentBundle:Section')->findSectionByAlias($alias);
    }
}