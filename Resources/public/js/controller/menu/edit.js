function AdminMenuEditCtrl($scope, $http, $state, $stateParams, $timeout, SweetAlert, $filter){
    angular.extend($scope, new EditCtrl($scope, $http, $state, $stateParams, $timeout, SweetAlert, $filter));
    $scope.prefix = 'admin/menu';
    $scope.statelink.list = 'content.menu_list';
    $scope.statelink.new = 'content.menu_new';
    $scope.breadcrumbs.title = 'MENU.EDIT';
    $scope.breadcrumbs.path = [{title: 'MENU.LIST', url: 'content.menu_list'}];

    var Form = new AdminMenuForm($scope);

    $scope.Get.Tabs = Form.GetTabs;

    $scope.Init();
}
AdminMenuEditCtrl.$inject = ['$scope', '$http', '$state', '$stateParams', '$timeout', 'SweetAlert', '$filter'];
angular.module('PiZone.Content').controller('AdminMenuEditCtrl', AdminMenuEditCtrl);