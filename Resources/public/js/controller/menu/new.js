function AdminMenuNewCtrl($scope, $http, $state, $timeout, SweetAlert, $filter){
    angular.extend($scope, new NewCtrl($scope, $http, $state, $timeout, SweetAlert, $filter));
    $scope.prefix = 'admin/menu';
    $scope.statelink.list = 'content.menu_list';
    $scope.statelink.new = 'content.menu_new';
    $scope.statelink.edit = 'content.menu_edit';
    $scope.breadcrumbs.title = 'MENU.NEW';
    $scope.breadcrumbs.path = [{title: 'MENU.LIST', url: 'content.menu_list'}];

    var Form = new AdminMenuForm($scope);

    $scope.Get.Tabs = Form.GetTabs;

    $scope.Init();
}
AdminMenuNewCtrl.$inject = ['$scope', '$http', '$state', '$timeout', 'SweetAlert', '$filter'];
angular.module('PiZone.Content').controller('AdminMenuNewCtrl', AdminMenuNewCtrl);