function ModalMenuItemEditCtrl($scope, $http, $state, $stateParams, $timeout, SweetAlert, $filter, $modalInstance, menuId, id){
    angular.extend($scope, new EditCtrl($scope, $http, $state, $stateParams, $timeout, SweetAlert, $filter));
    $scope.prefix = 'admin/menu_item';
    $scope.id = id;
    $scope.formId = 'menuItemFormId';

    var Form = new MenuItemForm($scope, menuId);

    $scope.Get.Fields = Form.GetFields;
    $scope.Callback.Submit.Success = CallbackSubmitSuccess;
    $scope.Click.Cancel = ClickCancel;

    function CallbackSubmitSuccess(message){
        $modalInstance.close();
        $scope.ViewNotify('submit', 'success');
    }

    function ClickCancel(){
        $modalInstance.close();
    }

    $scope.Init();
}
ModalMenuItemEditCtrl.$inject = ['$scope', '$http', '$state', '$stateParams', '$timeout', 'SweetAlert', '$filter', '$modalInstance', 'menuId', 'id'];
angular.module('PiZone.Content').controller('ModalMenuItemEditCtrl', ModalMenuItemEditCtrl);