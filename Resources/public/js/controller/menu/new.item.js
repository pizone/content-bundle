function ModalMenuItemNewCtrl($scope, $http, $state, $timeout, SweetAlert, $filter, $modalInstance, menuId, callback){
    angular.extend($scope, new NewCtrl($scope, $http, $state, $timeout, SweetAlert, $filter));
    $scope.prefix = 'admin/menu_item';
    $scope.formId = 'menuItemFormId';

    var Form = new MenuItemForm($scope, menuId);

    $scope.Get.Fields = Form.GetFields;
    $scope.Callback.Success = CallbackSubmitSuccess;
    $scope.Click.Cancel = ClickCancel;
    $scope.GetBaseQuery = GetBaseQuery;

    function GetBaseQuery(){
        return PiZoneConfig.apiPrefix + '/' + $scope.prefix + '/new?menuId=' + menuId;
    }

    function CallbackSubmitSuccess(message){
        $modalInstance.close();
        callback();
        $scope.ViewNotify('submit', 'success');
    }

    function ClickCancel(){
        $modalInstance.close();
    }

    $scope.Init();
}
ModalMenuItemNewCtrl.$inject = ['$scope', '$http', '$state', '$timeout', 'SweetAlert', '$filter', '$modalInstance', 'menuId', 'callback'];
angular.module('PiZone.Content').controller('ModalMenuItemNewCtrl', ModalMenuItemNewCtrl);