function AdminMenuListCtrl($scope, $http, $state, $stateParams, SweetAlert, $timeout, $filter, $cookieStore){
    angular.extend($scope, new ListCtrl($scope, $http, $state, $stateParams, SweetAlert, $timeout, $filter, $cookieStore));
    $scope.prefix = 'admin/menu';
    $scope.statelink.add = 'content.menu_new';
    $scope.statelink.edit = 'content.menu_edit';
    $scope.statelink.page = 'content.menu_list';
    $scope.breadcrumbs.title = 'MENU.LIST';
    $scope.model = AdminMenu;
    $scope.columns = [
        {show: true},
        {title: 'ID', name: 'id'},
        {title: 'MENU.FIELD.NAME', name: 'name'},
        {title: 'MENU.FIELD.ALIAS', name: 'alias'},
        {title: 'MENU.FIELD.IS_ACTIVE.LABEL', name: 'is_active'},
        {title: 'COMMON.ACTIONS'}
    ];
    $scope.showDefaultColumns = {
        all: [0, 1, 2, 3, 4, 5]
    };

    $scope.Callback.GetData = CreateFilters;

    function CreateFilters() {
        var data = $scope.filters;

        data.is_active.template = FieldDispatcher.GetLayout('simple_choice');
        $scope._token.filter = data._token;

        $scope.filters = [
            {icon: 'fa fa-cubes', field: data.all, show: true},
            {icon: 'fa fa-cube', field: data.name, show: data.name.value ? true : false},
            {icon: 'fa fa-codepen', field: data.alias, show: data.alias.value ? true : false},
            {icon: 'fa fa-eye', field: data.is_active, show: data.is_active.value >= 0 ? true: false},
        ];
    }

    $scope.Init();
}
AdminMenuListCtrl.$inject = ['$scope', '$http', '$state', '$stateParams', 'SweetAlert', '$timeout', '$filter', '$cookieStore'];
angular.module('PiZone.Content').controller('AdminMenuListCtrl', AdminMenuListCtrl);