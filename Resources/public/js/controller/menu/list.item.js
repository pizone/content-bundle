function MenuItemListCtrl($scope, $http, $state, $stateParams, SweetAlert, $timeout, $filter, $cookieStore, $modal){
    angular.extend($scope, new ListCtrl($scope, $http, $state, $stateParams, SweetAlert, $timeout, $filter, $cookieStore));
    $scope.menuId = $stateParams.id;
    $scope.prefix = 'admin/menu_item';
    $scope.breadcrumbs.title = 'MENU.ITEMS';
    $scope.perPage = 100;
    $scope.model = MenuItem;
    $scope.columns = [
        {show: true},
        {title: 'ID', name: 'id'},
        {title: 'MENU.FIELD.TITLE', name: 'title'},
        {title: 'MENU.FIELD.ALIAS', name: 'alias'},
        {title: 'MENU.FIELD.LINK', name: 'link'},
        {title: 'MENU.FIELD.IS_ACTIVE.LABEL', name: 'is_active'},
        {title: 'COMMON.ACTIONS'}
    ];
    $scope.showDefaultColumns = {
        all: [0, 1, 2, 3, 4, 5, 6]
    };

    $scope.GetBaseQuery = GetBaseQuery;
    $scope.Get.Data =ParseData;
    $scope.Click.Add = ClickAdd;

    function ClickAdd(){
        $modal.open({
            templateUrl: '/bundles/pizonecontent/tmpl/menu/new_item.html',
            size: 'lg',
            controller: ModalMenuItemNewCtrl,
            resolve: {
                menuId: function(){
                    return $scope.menuId;
                },
                callback: function() {
                    return $scope.Get.Layout;
                }
            }
        });
    }

    function ParseData(data){
        $scope.data = data;
        $scope.list = [];
        $.each(data.list, function(i, obj){
            $scope.list.push(new $scope.model($scope, $http, $state, i, obj, SweetAlert, $filter, $modal, $scope.menuId));
        });
        $scope.pager = data.pager;

        $scope.Callback.GetData();
        $scope.ShowColumns();
    }

    function GetBaseQuery(){
        return PiZoneConfig.apiPrefix + '/' + $scope.prefix  + '?menuId=' + $scope.menuId;
    }

    $scope.Init();
}
MenuItemListCtrl.$inject = ['$scope', '$http', '$state', '$stateParams', 'SweetAlert', '$timeout', '$filter', '$cookieStore', '$modal'];
angular.module('PiZone.Content').controller('MenuItemListCtrl', MenuItemListCtrl);