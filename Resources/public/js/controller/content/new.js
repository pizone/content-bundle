function AdminContentNewCtrl($scope, $http, $state, $timeout, SweetAlert, $filter){
    angular.extend($scope, new NewCtrl($scope, $http, $state, $timeout, SweetAlert, $filter));
    $scope.prefix = 'admin/content';
    $scope.statelink.list = 'content.content_list';
    $scope.statelink.new = 'content.content_new';
    $scope.statelink.edit = 'content.content_edit';
    $scope.breadcrumbs.title = 'CONTENT.NEW';
    $scope.breadcrumbs.path = [{title: 'CONTENT.LIST', url: 'content.content_list'}];

    var Form = new AdminContentForm($scope, $timeout);

    $scope.Get.Tabs = Form.GetTabs;

    $scope.Init();
}
AdminContentNewCtrl.$inject = ['$scope', '$http', '$state', '$timeout', 'SweetAlert', '$filter'];
angular.module('PiZone.Content').controller('AdminContentNewCtrl', AdminContentNewCtrl);