function AdminContentListCtrl($scope, $http, $state, $stateParams, SweetAlert, $timeout, $filter, $cookieStore){
    angular.extend($scope, new ListCtrl($scope, $http, $state, $stateParams, SweetAlert, $timeout, $filter, $cookieStore));
    $scope.prefix = 'admin/content';
    $scope.statelink.add = 'content.content_new';
    $scope.statelink.edit = 'content.content_edit';
    $scope.statelink.page = 'content.content_list';
    $scope.breadcrumbs.title = 'CONTENT.LIST';
    $scope.model = AdminContent;
    $scope.columns = [
        {show: true},
        {title: 'ID', name: 'id'},
        {title: 'CONTENT.FIELD.TITLE', name: 'title'},
        {title: 'CONTENT.FIELD.ALIAS', name: 'alias'},
        {title: 'CONTENT.FIELD.IMAGE', name: 'image'},
        {title: 'CONTENT.FIELD.ANONS', name: 'anons'},
        {title: 'CONTENT.FIELD.LAYOUT', name: 'layout'},
        {title: 'CONTENT.FIELD.SECTION', name: 'section'},
        {title: 'CONTENT.FIELD.PUBLISH_FROM', name: 'publish_at_from'},
        {title: 'CONTENT.FIELD.PUBLISH_TO', name: 'publish_at_to'},
        {title: 'CONTENT.FIELD.CREATED_AT', name: 'created_at'},
        {title: 'CONTENT.FIELD.UPDATED_AT', name: 'updated_at'},
        {title: 'CONTENT.FIELD.IS_ACTIVE.LABEL', name: 'is_active'},
        {title: 'COMMON.ACTIONS'}
    ];
    $scope.showDefaultColumns = {
        all: [0, 1, 2, 3, 8, 9, 12, 13]
    };

    $scope.Callback.GetData = CreateFilters;

    function CreateFilters() {
        var data = $scope.filters;
        data.is_active.template = FieldDispatcher.GetLayout('simple_choice');
        data.created_at.type = 'date_range';
        data.created_at.template = FieldDispatcher.GetLayout('date_range');
        data.updated_at.type = 'date_range';
        data.updated_at.template = FieldDispatcher.GetLayout('date_range');
        data.publish_at.type = 'date_range';
        data.publish_at.template = FieldDispatcher.GetLayout('date_range');

        $scope._token.filter = data._token;

        $scope.filters = [
            {icon: 'fa fa-cubes', field: data.all, show: true},
            {icon: 'fa fa-cube', field: data.title, show: data.title.value ? true : false},
            {icon: 'fa fa-codepen', field: data.alias, show: data.alias.value ? true : false},
            {icon: 'fa fa-cubes', field: data.section, show: data.section.value ? true: false},
            {icon: 'fa fa-file', field: data.anons, show: data.anons.value ? true: false},
            {icon: 'fa fa-calendar', field: data.publish_at, show: $scope.ShowDateFilter(data.publish_at.form)},
            {icon: 'fa fa-clock', field: data.created_at, show: $scope.ShowDateFilter(data.created_at.form)},
            {icon: 'fa fa-clock', field: data.updated_at, show: $scope.ShowDateFilter(data.updated_at.form)},
            {icon: 'fa fa-eye', field: data.is_active, show: data.is_active.value !== null ? true: false},
        ];
    }

    $scope.Init();
}
AdminContentListCtrl.$inject = ['$scope', '$http', '$state', '$stateParams', 'SweetAlert', '$timeout', '$filter', '$cookieStore'];
angular.module('PiZone.Content').controller('AdminContentListCtrl', AdminContentListCtrl);