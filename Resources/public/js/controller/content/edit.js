function AdminContentEditCtrl($scope, $http, $state, $stateParams, $timeout, SweetAlert, $filter){
    angular.extend($scope, new EditCtrl($scope, $http, $state, $stateParams, $timeout, SweetAlert, $filter));
    $scope.prefix = 'admin/content';
    $scope.statelink.list = 'content.content_list';
    $scope.statelink.new = 'content.content_new';
    $scope.breadcrumbs.title = 'CONTENT.EDIT';
    $scope.breadcrumbs.path = [{title: 'CONTENT.LIST', url: 'content.content_list'}];
    $scope.activeTab = {
        0: $stateParams.tabId === 0 ? true : false,
        1: $stateParams.tabId == 1 ? true : false,
        2: $stateParams.tabId == 2 ? true : false,
        3: $stateParams.tabId == 3 ? true : false
    };

    var Form = new AdminContentForm($scope, $timeout);

    $scope.Get.Tabs = Form.GetTabs;

    $scope.Init();
}
AdminContentEditCtrl.$inject = ['$scope', '$http', '$state', '$stateParams', '$timeout', 'SweetAlert', '$filter'];
angular.module('PiZone.Content').controller('AdminContentEditCtrl', AdminContentEditCtrl);