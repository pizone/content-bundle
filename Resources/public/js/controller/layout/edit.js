function AdminLayoutEditCtrl($scope, $http, $state, $stateParams, $timeout, SweetAlert, $filter){
    angular.extend($scope, new EditCtrl($scope, $http, $state, $stateParams, $timeout, SweetAlert, $filter));
    $scope.prefix = 'admin/layout';
    $scope.statelink.list = 'content.layout_list';
    $scope.statelink.new = 'content.layout_new';
    $scope.breadcrumbs.title = 'LAYOUT.EDIT';
    $scope.breadcrumbs.path = [{title: 'LAYOUT.LIST', url: 'content.layout_list'}];

    var Form = new AdminLayoutForm($scope);

    $scope.Get.Fields = Form.GetFields;

    $scope.Init();
}
AdminLayoutEditCtrl.$inject = ['$scope', '$http', '$state', '$stateParams', '$timeout', 'SweetAlert', '$filter'];
angular.module('PiZone.Content').controller('AdminLayoutEditCtrl', AdminLayoutEditCtrl);