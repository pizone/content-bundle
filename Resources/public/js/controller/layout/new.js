function AdminLayoutNewCtrl($scope, $http, $state, $timeout, SweetAlert, $filter){
    angular.extend($scope, new NewCtrl($scope, $http, $state, $timeout, SweetAlert, $filter));
    $scope.prefix = 'admin/layout';
    $scope.statelink.list = 'content.layout_list';
    $scope.statelink.new = 'content.layout_new';
    $scope.statelink.edit = 'content.layout_edit';
    $scope.breadcrumbs.title = 'LAYOUT.NEW';
    $scope.breadcrumbs.path = [{title: 'LAYOUT.LIST', url: 'content.layout_list'}];

    var Form = new AdminLayoutForm($scope);

    $scope.Get.Fields = Form.GetFields;

    $scope.Init();
}
AdminLayoutNewCtrl.$inject = ['$scope', '$http', '$state', '$timeout', 'SweetAlert', '$filter'];
angular.module('PiZone.Content').controller('AdminLayoutNewCtrl', AdminLayoutNewCtrl);