function AdminLayoutListCtrl($scope, $http, $state, $stateParams, SweetAlert, $timeout, $filter, $cookieStore){
    angular.extend($scope, new ListCtrl($scope, $http, $state, $stateParams, SweetAlert, $timeout, $filter, $cookieStore));
    $scope.prefix = 'admin/layout';
    $scope.statelink.add = 'content.layout_new';
    $scope.statelink.edit = 'content.layout_edit';
    $scope.statelink.page = 'content.layout_list';
    $scope.breadcrumbs.title = 'LAYOUT.LIST';
    $scope.model = AdminLayout;
    $scope.columns = [
        {show: true},
        {title: 'ID', name: 'id'},
        {title: 'LAYOUT.FIELD.NAME', name: 'name'},
        {title: 'LAYOUT.FIELD.DESCRIPTION', name: 'description'},
        {title: 'LAYOUT.FIELD.FILE', name: 'file'},
        {title: 'LAYOUT.FIELD.IS_ACTIVE.LABEL', name: 'is_active'},
        {title: 'COMMON.ACTIONS'}
    ];
    $scope.showDefaultColumns = {
        all: [0, 1, 2, 3, 4, 5, 6]
    };

    $scope.Callback.GetData = CreateFilters;

    function CreateFilters() {
        var data = $scope.filters;

        data.is_active.template = FieldDispatcher.GetLayout('simple_choice');
        $scope._token.filter = data._token;

        $scope.filters = [
            {icon: 'fa fa-cubes', field: data.all, show: true},
            {icon: 'fa fa-cube', field: data.name, show: data.name.value ? true : false},
            {icon: 'fa fa-comment', field: data.description, show: data.description.value ? true : false},
            {icon: 'fa fa-file', field: data.file, show: data.file.value ? true: false},
            {icon: 'fa fa-eye', field: data.is_active, show: data.is_active.value >= 0 ? true: false},
        ];
    }

    $scope.Init();
}
AdminLayoutListCtrl.$inject = ['$scope', '$http', '$state', '$stateParams', 'SweetAlert', '$timeout', '$filter', '$cookieStore'];
angular.module('PiZone.Content').controller('AdminLayoutListCtrl', AdminLayoutListCtrl);