function AdminSectionEditCtrl($scope, $http, $state, $stateParams, $timeout, SweetAlert, $filter){
    angular.extend($scope, new EditCtrl($scope, $http, $state, $stateParams, $timeout, SweetAlert, $filter));
    $scope.prefix = 'admin/section';
    $scope.statelink.list = 'content.section_list';
    $scope.statelink.new = 'content.section_new';
    $scope.breadcrumbs.title = 'SECTION.EDIT';
    $scope.breadcrumbs.path = [{title: 'SECTION.LIST', url: 'content.section_list'}];
    $scope.activeTab = {
        0: $stateParams.tabId === 0 ? true : false,
        1: $stateParams.tabId == 1 ? true : false,
        2: $stateParams.tabId == 2 ? true : false,
        3: $stateParams.tabId == 3 ? true : false
    };

    var Form = new AdminSectionForm($scope, $timeout);

    $scope.Get.Tabs = Form.GetTabs;

    $scope.Init();
}
AdminSectionEditCtrl.$inject = ['$scope', '$http', '$state', '$stateParams', '$timeout', 'SweetAlert', '$filter'];
angular.module('PiZone.Content').controller('AdminSectionEditCtrl', AdminSectionEditCtrl);