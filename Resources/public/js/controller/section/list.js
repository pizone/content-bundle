function AdminSectionListCtrl($scope, $http, $state, $stateParams, SweetAlert, $timeout, $filter, $cookieStore){
    angular.extend($scope, new ListCtrl($scope, $http, $state, $stateParams, SweetAlert, $timeout, $filter, $cookieStore));
    $scope.prefix = 'admin/section';
    $scope.statelink.add = 'content.section_new';
    $scope.statelink.edit = 'content.section_edit';
    $scope.statelink.page = 'content.section_list';
    $scope.breadcrumbs.title = 'SECTION.LIST';
    $scope.model = AdminSection;
    $scope.columns = [
        {show: true},
        {title: 'ID', name: 'id'},
        {title: 'SECTION.FIELD.IMAGE'},
        {title: 'SECTION.FIELD.NAME', name: 'name'},
        {title: 'SECTION.FIELD.ALIAS', name: 'alias'},
        {title: 'SECTION.FIELD.DESCRIPTION', name: 'description'},
        {title: 'SECTION.FIELD.ANONS', name: 'anons'},
        {title: 'SECTION.FIELD.IS_ACTIVE.LABEL', name: 'is_active'},
        {title: 'COMMON.ACTIONS'}
    ];
    $scope.showDefaultColumns = {
        all: [0, 1, 3, 4, 5, 7, 8]
    };

    $scope.Callback.GetData = CreateFilters;

    function CreateFilters() {
        var data = $scope.filters;

        data.is_active.template = FieldDispatcher.GetLayout('simple_choice');
        $scope._token.filter = data._token;

        $scope.filters = [
            {icon: 'fa fa-cubes', field: data.all, show: true},
            {icon: 'fa fa-cube', field: data.name, show: data.name.value ? true : false},
            {icon: 'fa fa-codepen', field: data.alias, show: data.alias.value ? true : false},
            {icon: 'fa fa-comment', field: data.description, show: data.description.value ? true : false},
            {icon: 'fa fa-file', field: data.anons, show: data.anons.value ? true : false},
            {icon: 'fa fa-cubes', field: data.parent, show: data.parent.value ? true: false},
            {icon: 'fa fa-eye', field: data.is_active, show: data.is_active.value !== null ? true: false},
        ];
    }

    $scope.Init();
}
AdminSectionListCtrl.$inject = ['$scope', '$http', '$state', '$stateParams', 'SweetAlert', '$timeout', '$filter', '$cookieStore'];
angular.module('PiZone.Content').controller('AdminSectionListCtrl', AdminSectionListCtrl);