function AdminSectionNewCtrl($scope, $http, $state, $timeout, SweetAlert, $filter){
    angular.extend($scope, new NewCtrl($scope, $http, $state, $timeout, SweetAlert, $filter));
    $scope.prefix = 'admin/section';
    $scope.statelink.list = 'content.section_list';
    $scope.statelink.new = 'content.section_new';
    $scope.statelink.edit = 'content.section_edit';
    $scope.breadcrumbs.title = 'SECTION.NEW';
    $scope.breadcrumbs.path = [{title: 'SECTION.LIST', url: 'content.section_list'}];

    var Form = new AdminSectionForm($scope, $timeout);

    $scope.Get.Tabs = Form.GetTabs;

    $scope.Init();
}
AdminSectionNewCtrl.$inject = ['$scope', '$http', '$state', '$timeout', 'SweetAlert', '$filter'];
angular.module('PiZone.Content').controller('AdminSectionNewCtrl', AdminSectionNewCtrl);