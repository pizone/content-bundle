function AdminWebItemListCtrl($scope, $http, $state, $stateParams, SweetAlert, $timeout, $filter, $cookieStore){
    angular.extend($scope, new ListCtrl($scope, $http, $state, $stateParams, SweetAlert, $timeout, $filter, $cookieStore));
    $scope.prefix = 'admin/webitem';
    $scope.statelink.add = 'content.webitem_new';
    $scope.statelink.edit = 'content.webitem_edit';
    $scope.statelink.page = 'content.webitem_list';
    $scope.breadcrumbs.title = 'WEBITEM.LIST';
    $scope.model = AdminWebItem;
    $scope.columns = [
        {show: true},
        {title: 'ID', name: 'id'},
        {title: 'WEBITEM.FIELD.ALIAS', name: 'alias'},
        {title: 'WEBITEM.FIELD.DESCRIPTION', name: 'description'},
        {title: 'WEBITEM.FIELD.IS_ACTIVE.LABEL', name: 'is_active'},
        {title: 'COMMON.ACTIONS'}
    ];
    $scope.showDefaultColumns = {
        all: [0, 1, 2, 3, 4, 5]
    };

    $scope.Callback.GetData = CreateFilters;

    function CreateFilters() {
        var data = $scope.filters;

        data.is_active.template = FieldDispatcher.GetLayout('simple_choice');
        $scope._token.filter = data._token;

        $scope.filters = [
            {icon: 'fa fa-cubes', field: data.all, show: true},
            {icon: 'fa fa-cube', field: data.alias, show: data.alias.value ? true : false},
            {icon: 'fa fa-comment', field: data.description, show: data.description.value ? true : false},
            {icon: 'fa fa-file', field: data.content, show: data.content.value ? true: false},
            {icon: 'fa fa-eye', field: data.is_active, show: data.is_active.value >= 0 ? true: false},
        ];
    }

    $scope.Init();
}
AdminWebItemListCtrl.$inject = ['$scope', '$http', '$state', '$stateParams', 'SweetAlert', '$timeout', '$filter', '$cookieStore'];
angular.module('PiZone.Content').controller('AdminWebItemListCtrl', AdminWebItemListCtrl);