function AdminWebItemEditCtrl($scope, $http, $state, $stateParams, $timeout, SweetAlert, $filter){
    angular.extend($scope, new EditCtrl($scope, $http, $state, $stateParams, $timeout, SweetAlert, $filter));
    $scope.prefix = 'admin/webitem';
    $scope.statelink.list = 'content.webitem_list';
    $scope.statelink.new = 'content.webitem_new';
    $scope.breadcrumbs.title = 'WEBITEM.EDIT';
    $scope.breadcrumbs.path = [{title: 'WEBITEM.LIST', url: 'content.webitem_list'}];

    var Form = new AdminWebItemForm($scope, $timeout);

    $scope.Get.Fields = Form.GetFields;

    $scope.Init();
}
AdminWebItemEditCtrl.$inject = ['$scope', '$http', '$state', '$stateParams', '$timeout', 'SweetAlert', '$filter'];
angular.module('PiZone.Content').controller('AdminWebItemEditCtrl', AdminWebItemEditCtrl);