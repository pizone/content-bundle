function AdminWebItemNewCtrl($scope, $http, $state, $timeout, SweetAlert, $filter){
    angular.extend($scope, new NewCtrl($scope, $http, $state, $timeout, SweetAlert, $filter));
    $scope.prefix = 'admin/webitem';
    $scope.statelink.list = 'content.webitem_list';
    $scope.statelink.new = 'content.webitem_new';
    $scope.statelink.edit = 'content.webitem_edit';
    $scope.breadcrumbs.title = 'WEBITEM.NEW';
    $scope.breadcrumbs.path = [{title: 'WEBITEM.LIST', url: 'content.webitem_list'}];

    var Form = new AdminWebItemForm($scope, $timeout);

    $scope.Get.Fields = Form.GetFields;

    $scope.Init();
}
AdminWebItemNewCtrl.$inject = ['$scope', '$http', '$state', '$timeout', 'SweetAlert', '$filter'];
angular.module('PiZone.Content').controller('AdminWebItemNewCtrl', AdminWebItemNewCtrl);