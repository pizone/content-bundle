
function StateConfigPZContent($stateProvider, $ocLazyLoadProvider, IdleProvider) {

    // Configure Idle settings
    IdleProvider.idle(5); // in seconds
    IdleProvider.timeout(120); // in seconds

    $ocLazyLoadProvider.config({
        // Set to true if you want to see what and when is dynamically loaded
        debug: false
    });

    $stateProvider
        .state('content', {
            abstract: true,
            url: "",
            templateUrl:  "/bundles/pizoneadmin/tmpl/common/_admin_content.html"
        })
        .state('content.content_new', GetStateConfig('content', 'new'))
        .state('content.content_edit', GetStateConfig('content', 'edit'))
        .state('content.content_list', GetStateConfig('content', 'list'))
        .state('content.layout_new', GetStateConfig('layout', 'new'))
        .state('content.layout_edit', GetStateConfig('layout', 'edit'))
        .state('content.layout_list', GetStateConfig('layout', 'list'))
        .state('content.section_new', GetStateConfig('section', 'new'))
        .state('content.section_edit', GetStateConfig('section', 'edit'))
        .state('content.section_list', GetStateConfig('section', 'list'))
        .state('content.webitem_new', GetStateConfig('webitem', 'new'))
        .state('content.webitem_edit', GetStateConfig('webitem', 'edit'))
        .state('content.webitem_list', GetStateConfig('webitem', 'list'))
        .state('content.menu_new', GetStateConfig('menu', 'new'))
        .state('content.menu_edit', GetStateConfig('menu', 'edit'))
        .state('content.menu_list', GetStateConfig('menu', 'list'))
    ;

    var collections = new LazyLoadCollection();

    function GetStateConfig(name, action){
        var url = '',
            tmpl = '',
            path = "/bundles/pizonecontent/tmpl/";
        if(action == 'list'){
            url = "/" + name + "/:pageId?sort&order_by&perPage";
            tmpl = path + name + '/index.html';
        }
        else if (action == 'new'){
            url = "/" + name + "/new";
            tmpl = path + name + '/new.html';
        }
        else if(action == 'edit'){
            url = "/" + name + "/edit/:id";
            tmpl = path + name + '/edit.html';
        }
        return {
            url: url,
                templateUrl: tmpl,
                resolve: {
                loadPlugin: function ($ocLazyLoad) {
                    if(action == 'list')
                        return $ocLazyLoad.load(collections.GetList());
                    else
                        return $ocLazyLoad.load(collections.GetEdit());
                }
            }
        };
    }
}
StateConfigPZContent.$inject = ['$stateProvider', '$ocLazyLoadProvider', 'IdleProvider'];

angular
    .module('PiZone.Content')
    .config(StateConfigPZContent);
