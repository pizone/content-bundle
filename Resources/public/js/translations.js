function ContentTranslationConfig($translateProvider) {
    $translateProvider
        .translations('en', {
            CONTENT: enContentPZTranslates,
            LAYOUT: enLayoutPZTranslates,
            SECTION: enSectionPZTranslates,
            WEBITEM: enWebItemPZTranslates,
            MENU: enMenuPZTranslates
        })
        .translations('ru', {
            CONTENT: ruContentPZTranslates,
            LAYOUT: ruLayoutPZTranslates,
            SECTION: ruSectionPZTranslates,
            WEBITEM: ruWebItemPZTranslates,
            MENU: ruMenuPZTranslates
        });
}
ContentTranslationConfig.$inject = ['$translateProvider'];

angular
    .module('PiZone.Content')
    .config(ContentTranslationConfig);
