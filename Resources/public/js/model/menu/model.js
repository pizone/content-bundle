function AdminMenu($scope, $http, $state, i, data, SweetAlert, $filter){
    var self = new Model($scope, $http, $state, i, data, SweetAlert, $filter);
    self.prefix = 'admin/menu';
    self.statelink.edit = 'content.menu_edit';

    angular.extend(this, self);
}