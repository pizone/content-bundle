function MenuItem($scope, $http, $state, i, data, SweetAlert, $filter, $modal, menuId){
    var self = new Model($scope, $http, $state, i, data, SweetAlert, $filter);
    self.prefix = 'admin/menu_item';
    self.Click.Edit = ClickEdit;

    function ClickEdit(){
        $modal.open({
            templateUrl: '/bundles/pizonecontent/tmpl/menu/edit_item.html',
            size: 'lg',
            controller: ModalMenuItemEditCtrl,
            resolve: {
                menuId: function(){
                    return menuId;
                },
                id: function() {
                    return self.id;
                }
            }
        });
    }
    angular.extend(this, self);
}