function MenuItemForm($scope, menuId){
    var self = this;
    self.GetFields = GetFields;

    function GetFields(data){
        $scope.breadcrumbs.param = data.title.value;

        var requiredMess = 'MENU.MESSAGE.REQUIRED.';

        data.menu.template = FieldDispatcher.GetLayout('hidden');
        data.menu.type = 'hidden';
        data.menu.value = menuId;
        $scope.fields =
            [
                [
                    {size: 0, icon: '', field: data._token},
                    {size: 0, icon: '', field: data.menu},
                    {size: 6, icon: 'fa fa-folder-open', field: data.parent},
                    {size: 6, icon: 'fa fa-eye', field: data.is_active}
                ],
                [
                    {size: 6, icon: 'fa fa-cube', field: data.title, assert: [
                        {key: 'notNull', message: requiredMess  + 'TITLE'}
                    ]},
                    {size: 6, icon: 'fa fa-codepen', field: data.alias, assert: [
                        {key: 'notNull', message: requiredMess  + 'ALIAS'}
                    ]}
                ],
                [
                    {size: 12, icon: 'fa fa-link', field: data.link}
                ]
            ];
    }
}