function AdminMenuForm($scope) {
    var self = this;
    self.GetTabs = GetTabs;

    function GetTabs(data){

        $scope.breadcrumbs.param = data.name.value;
        var requiredMess = 'MENU.MESSAGE.REQUIRED.',
            regexpMess = 'MENU.MESSAGE.REGEXP.';

        $scope.tabs = [
            {
                title: 'MENU.TAB.GENERAL.TITLE',
                description: '',
                form: true,
                valid: true,
                groups: [
                    [
                        {size: 0, icon: '', field: data._token},
                        {size: 12, icon: 'fa fa-eye', field: data.is_active}
                    ],
                    [
                        {size: 6, icon: 'fa fa-cube', field: data.name, assert: [
                            {key: 'notNull', message: requiredMess  + 'NAME'}
                        ]},
                        {size: 6, icon: 'fa fa-codepen', field: data.alias, assert: [
                            {key: 'notNull', message: requiredMess  + 'ALIAS'}
                        ]}
                    ]
                ]
            },
            {
                title: 'MENU.TAB.ITEMS.TITLE',
                description: 'MENU.TAB.ITEMS.DESCRIPTION',
                valid: true,
                form: false,
                getContent: function () {
                    $scope.tabs[1].template = '/bundles/pizonecontent/tmpl/menu/index_item.html';
                },
                template: ''
            }
        ];
    }
}