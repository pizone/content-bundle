function AdminWebItem($scope, $http, $state, i, data, SweetAlert, $filter){
    var self = new Model($scope, $http, $state, i, data, SweetAlert, $filter);
    self.prefix = 'admin/webitem';
    self.statelink.edit = 'content.webitem_edit';

    angular.extend(this, self);
}