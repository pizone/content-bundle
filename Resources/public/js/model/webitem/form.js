function AdminWebItemForm($scope, $timeout) {
    var self = this;
    self.GetFields = GetFields;

    function GetFields(data){
        $scope.breadcrumbs.param = data.alias.value;

        var requiredMess = 'WEBITEM.MESSAGE.REQUIRED.';

        data.content.template = FieldDispatcher.GetLayout('editor');

        $scope.fields =
            [
                [
                    {size: 0, icon: '', field: data._token},
                    {size: 8, icon: 'fa fa-cube', field: data.alias, assert: [
                        {key: 'notNull', message: requiredMess  + 'ALIAS'}
                    ]},
                    {size: 4, icon: 'fa fa-eye', field: data.is_active}
                ],
                [
                    {size: 12, icon: 'fa fa-comment', field: data.description}
                ],
                [
                    {size: 12, icon: 'fa fa-file', field: data.content, assert: [
                        {key: 'notNull', message: requiredMess  + 'CONTENT'}
                    ]}
                ],
                [
                    {size: 12, icon: 'fa fa-edit', field: data.show_editor_content}
                ]
            ];

        Watch();
    }

    function SetLayout(type){
        $timeout(function() {
            $scope.fields[2][0].field.template = FieldDispatcher.GetLayout(type);
        });
    }

    function Watch(){
        $scope.$watch('fields[3][0].field.checked', function(val){
            if(val)
                SetLayout('editor');
            else
                SetLayout('textarea');
        }, true);
    }
}