function AdminLayout($scope, $http, $state, i, data, SweetAlert, $filter){
    var self = new Model($scope, $http, $state, i, data, SweetAlert, $filter);
    self.prefix = 'admin/layout';
    self.statelink.edit = 'content.layout_edit';

    angular.extend(this, self);
}