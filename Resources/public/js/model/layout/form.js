function AdminLayoutForm($scope) {
    var self = this;
    self.GetFields = GetFields;

    function GetFields(data){
        $scope.breadcrumbs.param = data.name.value;

        var requiredMess = 'LAYOUT.MESSAGE.REQUIRED.';

        $scope.fields =
            [
                [
                    {size: 0, icon: '', field: data._token},
                    {size: 8, icon: 'fa fa-cube', field: data.name, assert: [
                        {key: 'notNull', message: requiredMess  + 'NAME'}
                    ]},
                    {size: 4, icon: 'fa fa-eye', field: data.is_active}
                ],
                [
                    {size: 12, icon: 'fa fa-comment', field: data.description}
                ],
                [
                    {size: 12, icon: 'fa fa-file', field: data.file, assert: [
                        {key: 'notNull', message: requiredMess  + 'FILE'}
                    ]}
                ]
            ];
    }
}