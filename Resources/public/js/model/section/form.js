function AdminSectionForm($scope, $timeout) {
    var self = this;
    self.GetTabs = GetTabs;

    function GetTabs(data){
        $scope.breadcrumbs.param = data.name.value;

        var requiredMess = 'SECTION.MESSAGE.REQUIRED.';

        data.image.remove = data.delete_image;

        $scope.tabs = [
            {
                title: 'SECTION.TAB.GENERAL.TITLE',
                description: '',
                form: true,
                valid: true,
                groups: [
                    [
                        {size: 0, icon: '', field: data._token},
                        {size: 6, icon: 'fa fa-cubes', field: data.parent},
                        {size: 6, icon: 'fa fa-eye', field: data.is_active}
                    ],
                    [
                        {size: 6, icon: 'fa fa-cube', field: data.name, assert: [
                            {key: 'notNull', message: requiredMess  + 'NAME'}
                        ]},
                        {size: 6, icon: 'fa fa-cube', field: data.alias, assert: [
                            {key: 'notNull', message: requiredMess  + 'ALIAS'}
                        ]}
                    ],
                    [
                        {size: 12, icon: 'fa fa-comment', field: data.description}
                    ]
                ]
            },
            {
                title: 'SECTION.TAB.CONTENT.TITLE',
                description: '',
                form: true,
                valid: true,
                groups: [
                    [
                        {size: 2, icon: 'fa fa-picture', field: data.image}
                    ],
                    [
                        {size: 12, icon: 'fa fa-file', field: data.anons}
                    ],
                    [
                        {size: 12, icon: 'fa fa-edit', field: data.show_editor_anons}
                    ]
                ]
            },
            {
                title: 'SECTION.TAB.META.TITLE',
                description: '',
                form: true,
                valid: true,
                groups: [
                    [
                        {size: 6, icon: 'fa fa-cube', field: data.meta.form.meta_title}
                    ],
                    [
                        {size: 6, icon: 'fa fa-key', field: data.meta.form.meta_keywords}
                    ],
                    [
                        {size: 6, icon: 'fa fa-comment', field: data.meta.form.meta_description}
                    ],
                    [
                        {size: 6, icon: 'fa fa-code', field: data.meta.form.more_scripts}
                    ]
                ]
            }
        ];

        Watch(1);
    }

    function SetLayout(tabsId, type){
        $timeout(function() {
            $scope.tabs[tabsId].groups[1][0].field.template = FieldDispatcher.GetLayout(type);
        });
    }

    function Watch(tabsId){
        $scope.$watch('tabs[' + tabsId + '].groups[2][0].field.checked', function(val){
            if(val)
                SetLayout(tabsId, 'editor');
            else
                SetLayout(tabsId, 'textarea');
        }, true);
    }
}