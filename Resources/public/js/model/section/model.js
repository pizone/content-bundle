function AdminSection($scope, $http, $state, i, data, SweetAlert, $filter){
    var self = new Model($scope, $http, $state, i, data, SweetAlert, $filter);
    self.prefix = 'admin/section';
    self.statelink.edit = 'content.section_edit';

    angular.extend(this, self);
}