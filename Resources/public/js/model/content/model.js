function AdminContent($scope, $http, $state, i, data, SweetAlert, $filter){
    var self = new Model($scope, $http, $state, i, data, SweetAlert, $filter);
    self.prefix = 'admin/content';
    self.statelink.edit = 'content.content_edit';

    angular.extend(this, self);
}