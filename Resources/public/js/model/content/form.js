function AdminContentForm($scope, $timeout) {
    var self = this;
    self.GetTabs = GetTabs;

    function GetTabs(data){

        $scope.breadcrumbs.param = data.title.value;
        var requiredMess = 'CONTENT.MESSAGE.REQUIRED.',
            regexpMess = 'CONTENT.MESSAGE.REGEXP.';

        data.image.remove = data.delete_image;
        data.big_image.remove = data.delete_big_image;

        $scope.tabs = [
            {
                title: 'CONTENT.TAB.GENERAL.TITLE',
                description: '',
                form: true,
                valid: true,
                groups: [
                    [
                        {size: 0, icon: '', field: data._token},
                        {size: 8, icon: 'fa fa-eye', field: data.is_active}
                    ],
                    [
                        {size: 3, icon: 'fa fa-calendar', field: data.publish_at_from},
                        {size: 3, icon: 'fa fa-calendar', field: data.publish_at_to}
                    ],
                    [
                        {size: 6, icon: 'fa fa-cube', field: data.title, assert: [
                            {key: 'notNull', message: requiredMess  + 'TITLE'}
                        ]},
                        {size: 6, icon: 'fa fa-codepen', field: data.alias, assert: [
                            {key: 'notNull', message: requiredMess  + 'ALIAS'}
                        ]}
                    ],
                    [
                        {size: 6, icon: 'fa fa-desktop', field: data.layout, assert: [
                            {key: 'notNull', message: requiredMess  + 'LAYOUT'}
                        ]},
                        {size: 6, icon: 'fa fa-folder-open', field: data.section}
                    ]
                ]
            },
            {
                title: 'CONTENT.TAB.ANONS.TITLE',
                description: '',
                form: true,
                valid: true,
                groups: [
                    [
                        {size: 2, icon: 'fa fa-picture', field: data.image}
                    ],
                    [
                        {size: 12, icon: 'fa fa-file', field: data.anons}
                    ],
                    [
                        {size: 12, icon: 'fa fa-edit', field: data.show_editor_anons}
                    ]
                ]
            },
            {
                title: 'CONTENT.TAB.CONTENT.TITLE',
                description: '',
                form: true,
                valid: true,
                groups: [
                    [
                        {size: 2, icon: 'fa fa-picture', field: data.big_image}
                    ],
                    [
                        {size: 12, icon: 'fa fa-file', field: data.content}
                    ],
                    [
                        {size: 12, icon: 'fa fa-edit', field: data.show_editor_content}
                    ]
                ]
            },
            {
                title: 'CONTENT.TAB.META.TITLE',
                description: '',
                form: true,
                valid: true,
                groups: [
                    [
                        {size: 6, icon: 'fa fa-cube', field: data.meta.form.meta_title}
                    ],
                    [
                        {size: 6, icon: 'fa fa-key', field: data.meta.form.meta_keywords}
                    ],
                    [
                        {size: 6, icon: 'fa fa-comment', field: data.meta.form.meta_description}
                    ],
                    [
                        {size: 6, icon: 'fa fa-code', field: data.meta.form.more_scripts}
                    ]
                ]
            }
        ];

        Watch(1);
        Watch(2);
    }

    function SetLayout(tabsId, type){
        $timeout(function() {
            $scope.tabs[tabsId].groups[1][0].field.template = FieldDispatcher.GetLayout(type);
        });
    }

    function Watch(tabsId){
        $scope.$watch('tabs[' + tabsId + '].groups[2][0].field.checked', function(val){
            if(val)
                SetLayout(tabsId, 'editor');
            else
                SetLayout(tabsId, 'textarea');
        }, true);
    }
}