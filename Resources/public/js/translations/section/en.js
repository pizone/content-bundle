var enSectionPZTranslates = {
    LIST: 'Sections',
    NEW: 'New section',
    EDIT: 'Edit section - "{{param}}"',
    TAB:{
        GENERAL: {
            TITLE: 'Main data'
        },
        CONTENT: {
            TITLE: 'Content'
        },
        META: {
            TITLE: 'SEO'
        }
    },
    FIELD: {
        NAME: 'Section name',
        ALIAS: 'Alias',
        DESCRIPTION: 'Description',
        PARENT: 'Parent section',
        IMAGE: 'Image',
        ANONS: 'Anons',
        EDITOR_ANONS: 'Use the visual editor',
        IS_ACTIVE: {
            LABEL: 'Is active?',
            NO: 'No',
            YES: 'Yes',
            ANY: 'Any'
        },
        META_TITLE: 'Meta title',
        META_KEYWORDS: 'Meta keywords',
        META_DESCRIPTION: 'Meta description',
        META_SCRIPTS: 'More scripts',
        ALL: 'Common search'
    },
    MESSAGE: {
        REQUIRED: {
            NAME: 'Please enter section name.',
            ALIAS: 'Please enter section alias.'
        }
    }
};