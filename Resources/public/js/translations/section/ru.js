var ruSectionPZTranslates = {
    LIST: 'Разделы',
    NEW: 'Новый раздел',
    EDIT: 'Редактировать раздел - "{{param}}"',
    TAB:{
        GENERAL: {
            TITLE: 'Основные данные'
        },
        CONTENT: {
            TITLE: 'Контент'
        },
        META: {
            TITLE: 'SEO'
        }
    },
    FIELD: {
        NAME: 'Название',
        ALIAS: 'Псевдоним',
        DESCRIPTION: 'Описание',
        PARENT: 'Родительский раздел',
        IMAGE: 'Изображение',
        ANONS: 'Анонс',
        EDITOR_ANONS: 'Использовать визуальный редактор',
        IS_ACTIVE: {
            LABEL: 'Активен?',
            NO: 'Нет',
            YES: 'Да',
            ANY: 'Любой'
        },
        META_TITLE: 'Meta title',
        META_KEYWORDS: 'Meta keywords',
        META_DESCRIPTION: 'Meta description',
        META_SCRIPTS: 'Дополнительные скрипты',
        ALL: 'Общий поиск'
    },
    MESSAGE: {
        REQUIRED: {
            NAME: 'Введите название раздела.',
            ALIAS: 'Введите псевдоним раздела.'
        }
    }
};