var enWebItemPZTranslates = {
    LIST: 'Web items',
    NEW: 'New web item',
    EDIT: 'Edit web item - "{{param}}"',
    FIELD: {
        ALIAS: 'Web item alias',
        DESCRIPTION: 'Description',
        CONTENT: 'Content',
        EDITOR_CONTENT: 'Use the visual editor',
        IS_ACTIVE: {
            LABEL: 'Is active?',
            NO: 'No',
            YES: 'Yes',
            ANY: 'Any'
        },
        ALL: 'Common search'
    },
    MESSAGE: {
        REQUIRED: {
            ALIAS: 'Please enter alias.',
            CONTENT: 'Please enter content.'
        }
    }
};