var ruWebItemPZTranslates = {
    LIST: 'Web вставки',
    NEW: 'Новая вставка',
    EDIT: 'Редактировать вставку - "{{param}}"',
    FIELD: {
        ALIAS: 'Название',
        DESCRIPTION: 'Описание',
        CONTENT: 'Контент',
        EDITOR_CONTENT: 'Использовать визуальный редактор',
        IS_ACTIVE: {
            LABEL: 'Активен?',
            NO: 'Нет',
            YES: 'Да',
            ANY: 'Любой'
        },
        ALL: 'Общий поиск'
    },
    MESSAGE: {
        REQUIRED: {
            ALIAS: 'Введите псевдоним вставки.',
            CONTENT: 'Введите контент вставки.'
        }
    }
};