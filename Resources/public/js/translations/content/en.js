var enContentPZTranslates = {
    LIST: 'Pages',
    NEW: 'New page',
    EDIT: 'Edit page - "{{param}}"',
    TAB:{
        GENERAL: {
            TITLE: 'Main data'
        },
        ANONS: {
            TITLE: 'Anons'
        },
        CONTENT: {
            TITLE: 'Content'
        },
        META: {
            TITLE: 'SEO'
        }
    },
    FIELD: {
        TITLE: 'Title',
        ALIAS: 'Alias',
        ANONS: 'Anons',
        EDITOR_ANONS: 'Use the visual editor',
        CONTENT: 'Content',
        EDITOR_CONTENT: 'Use the visual editor',
        IMAGE: 'Picture',
        LAYOUT: 'Layout',
        SECTION: 'Section',
        CREATED_AT: 'Date of creation',
        UPDATED_AT: 'Updated date',
        PUBLISH_FROM: 'Date (start)',
        PUBLISH_TO: 'Date (end)',
        PUBLISH_AT: 'Date of publication',
        IS_ACTIVE: {
            LABEL: 'Is active?',
            NO: 'No',
            YES: 'Yes',
            ANY: 'Any'
        },
        META_TITLE: 'Meta title',
        META_KEYWORDS: 'Meta keywords',
        META_DESCRIPTION: 'Meta description',
        META_SCRIPTS: 'More scripts',
        ALL: 'Common search'
    },
    MESSAGE: {
        REQUIRED: {
            TITLE: 'Please enter title.',
            ALIAS: 'Please enter alias.'
        }
    }
};