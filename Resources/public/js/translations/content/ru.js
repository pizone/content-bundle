var ruContentPZTranslates = {
    LIST: 'Страницы',
    NEW: 'Новая страница',
    EDIT: 'Редактировать страницу - "{{param}}"',
    TAB:{
        GENERAL: {
            TITLE: 'Основные данные'
        },
        ANONS: {
            TITLE: 'Анонс'
        },
        CONTENT: {
            TITLE: 'Контент'
        },
        META: {
            TITLE: 'SEO'
        }
    },
    FIELD: {
        TITLE: 'Заголовок',
        ALIAS: 'Псевдоним',
        ANONS: 'Анонс',
        EDITOR_ANONS: 'Использовать визуальный редактор',
        CONTENT: 'Контент',
        EDITOR_CONTENT: 'Использовать визуальный редактор',
        IMAGE: 'Изображение',
        LAYOUT: 'Шаблон',
        SECTION: 'Раздел',
        CREATED_AT: 'Дата создания',
        UPDATED_AT: 'Дата обновления',
        PUBLISH_FROM: 'Дата публикации(начало)',
        PUBLISH_TO: 'Дата публикации(конец)',
        PUBLISH_AT: 'Дата публикации',
        IS_ACTIVE: {
            LABEL: 'Активен?',
            NO: 'Нет',
            YES: 'Да',
            ANY: 'Любой'
        },
        META_TITLE: 'Meta title',
        META_KEYWORDS: 'Meta keywords',
        META_DESCRIPTION: 'Meta description',
        META_SCRIPTS: 'Дополнительные скрипты',
        ALL: 'Общий поиск'
    },
    MESSAGE: {
        REQUIRED: {
            TITLE: 'Введите название страницы.',
            ALIAS: 'Введите псевдоним страницы.'
        }
    }
};