var ruLayoutPZTranslates = {
    LIST: 'Шаблоны',
    NEW: 'Новый шаблон',
    EDIT: 'Редактировать шаблон - "{{param}}"',
    FIELD: {
        NAME: 'Название',
        DESCRIPTION: 'Описание',
        FILE: 'Файл',
        IS_ACTIVE: {
            LABEL: 'Активен?',
            NO: 'Нет',
            YES: 'Да',
            ANY: 'Любой'
        },
        ALL: 'Общий поиск'
    },
    MESSAGE: {
        REQUIRED: {
            NAME: 'Введите название шаблона.',
            TEMPLATE: 'Введите название файла.'
        }
    }
};