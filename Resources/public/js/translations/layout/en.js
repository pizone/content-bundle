var enLayoutPZTranslates = {
    LIST: 'Layouts',
    NEW: 'New layout',
    EDIT: 'Edit layout - "{{param}}"',
    FIELD: {
        NAME: 'Layout name',
        DESCRIPTION: 'Description',
        FILE: 'File',
        IS_ACTIVE: {
            LABEL: 'Is active?',
            NO: 'No',
            YES: 'Yes',
            ANY: 'Any'
        },
        ALL: 'Common search'
    },
    MESSAGE: {
        REQUIRED: {
            NAME: 'Please enter layout name.',
            TEMPLATE: 'Please enter file name.'
        }
    }
};