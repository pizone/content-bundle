var ruMenuPZTranslates = {
    LIST: 'Меню',
    NEW: 'Новое меню',
    NEW_ITEM: 'Новый раздел меню',
    EDIT: 'Редактировать меню - "{{param}}"',
    EDIT_ITEM: 'Редактировать элемент меню',
    TAB:{
        GENERAL: {
            TITLE: 'Данные меню'
        },
        ITEMS: {
            TITLE: 'Пункты меню'
        }
    },
    FIELD: {
        NAME: 'Название',
        TITLE: 'Заголовок',
        ALIAS: 'Псевдоним',
        LINK: 'Ссылка',
        PARENT: 'Родительский элемент',
        IS_ACTIVE: {
            LABEL: 'Активен?',
            NO: 'Нет',
            YES: 'Да',
            ANY: 'Любой'
        },
        ALL: 'Общий поиск'
    },
    MESSAGE: {
        REQUIRED: {
            NAME: 'Введите название меню.',
            ALIAS: 'Введите псевдоним меню.'
        }
    }
};