var enMenuPZTranslates = {
    LIST: 'Menus',
    NEW: 'New menu',
    NEW_ITEM: 'New menu item',
    EDIT: 'Edit menu - "{{param}}"',
    EDIT_ITEM: 'Edit menu item',
    TAB:{
        GENERAL: {
            TITLE: 'Main data'
        },
        ITEMS: {
            TITLE: 'Items'
        }
    },
    FIELD: {
        NAME: 'Menu name',
        TITLE: 'Title',
        ALIAS: 'Alias',
        LINK: 'Link',
        PARENT: 'Parent item',
        IS_ACTIVE: {
            LABEL: 'Is active?',
            NO: 'No',
            YES: 'Yes',
            ANY: 'Any'
        },
        ALL: 'Common search'
    },
    MESSAGE: {
        REQUIRED: {
            NAME: 'Please enter menu name.',
            ALIAS: 'Please enter menu alias.'
        }
    }
};