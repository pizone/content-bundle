module.exports = {
    // Опции
    options: {
        limit: 3
    },

    // Задачи разработки
    devFirstContent: [
        'jshint'
    ],
    devSecondContent: [
        'uglify'
    ],


    // Производственные задачи
    prodFirstContent: [
        'jshint'
    ],
    prodSecondContent: [
        'uglify'
    ]
};