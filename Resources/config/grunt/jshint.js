module.exports = {
    options: {
        reporter: require('jshint-stylish')
    },
    main: [
        'web/bundles/pizonecontent/js/**/*.js'
    ]
};