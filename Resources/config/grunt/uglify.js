module.exports = {
    my_target: {
        files: {
            'web/assetic/js/content.min.js': [
                'web/bundles/pizonecontent/js/app.js',
                'web/bundles/pizonecontent/js/translations/**/*.js',
                'web/bundles/pizonecontent/js/translations.js',
                // 'web/bundles/pizonecatalog/js/config.js',
                'web/bundles/pizonecontent/js/controller/**/*.js',
                'web/bundles/pizonecontent/js/model/**/*.js'
            ]
        }
    }
};